<?php
	if($erno) die();
	if(isset($proses)){
		/** koneksi ke database */
		try {
			$db 	= new PDO($PSPDO[0],$PSPDO[1],$PSPDO[2]);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch (PDOException $err){
			$mess = $err->getTrace();
			errorLog::errorDB(array($mess[0]['args'][0]));
			$mess = "Mungkin telah terjadi kesalahan pada database server, sehingga koneksi tidak bisa dilakukan.";
			$klas = "error";
		}
		
		try {
			$db->beginTransaction();
			$que	= "INSERT tr_trans_log(tr_id,tr_sts,tr_ip,kp_kode,kar_id) VALUES('"._TOKN."',4,INET_ATON('"._HOST."'),'"._KOTA."','"._USER."')";
			$st 	= $db->exec($que);
			if($st>0){
				$db->commit();
				errorLog::logDB(array($que));
				$mess = "Loket pembayaran telah ditutup. Tekan <b>Esc</b> untuk menutup pesan ini.";
				$klas = "success";
			}
		}
		catch (PDOException $err){
			$db->rollBack();
			$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses buka loket tidak bisa dilakukan.";
			errorLog::errorDB(array($err->getMessage()));
			errorLog::logMess(array($mess));
			$klas = "error";
		}
		unset($db);
?>
<input id="<?php echo $errorId; ?>" type="hidden" value="<?php echo $mess; ?>"/>
<?php
	}
	else{
		$que0	= "SELECT IFNULL(MAX(tr_sts),0) AS tr_sts FROM tr_trans_log WHERE kar_id='"._USER."' AND (tr_tgl BETWEEN CURDATE() AND NOW()) AND tr_sts<5";
		try{
			if(!$res0 = mysql_query($que0,$link)){
				throw new Exception(mysql_error($link));
			}
			else{
				$row0 	= mysql_fetch_array($res0);
				$tr_sts	= $row0['tr_sts'];
				unset($mess);
			}
		}
		catch (Exception $e){
			$mess = "Terjadi kesalahan pada sistem<br/>Nomor Tiket : ".substr(_TOKN,-4);
			errorLog::logMess(array($mess));
			errorLog::errorDB(array($e->getMessage()));
		}
		if(!$erno) mysql_close($link);
		
		if($tr_sts==4){
			$stsLoket	= "disabled";
			$mess		= "Loket sudah ditutup. Tekan <b>Esc</b> untuk menutup pesan ini.";
		}
		else if($tr_sts<3){
			$stsLoket 	= "disabled";
			$mess 		= "Loket belum dibuka. Tekan <b>Esc</b> untuk menutup pesan ini.";
		}
?>
<h2><?php echo _NAME; ?> - <?php echo $_SESSION['kp_ket']; ?></h2>
<input type="hidden" id="norefresh" 	value="1" />
<input type="hidden" id="<?php echo $errorId; ?>" value="<?php echo $mess; ?>" />
<input type="hidden" class="buka" 	name="appl_kode" 	value="<?php echo _KODE; 		?>"/>
<input type="hidden" class="buka" 	name="appl_name" 	value="<?php echo _NAME; 		?>"/>
<input type="hidden" class="buka" 	name="appl_file" 	value="<?php echo _FILE; 		?>"/>
<input type="hidden" class="buka" 	name="appl_proc" 	value="<?php echo _PROC; 		?>"/>
<input type="hidden" class="buka" 	name="errorId"   	value="<?php echo getToken();	?>"/>
<input type="hidden" class="buka"	name="targetUrl" 	value="<?php echo _PROC; 		?>"/>
<input type="hidden" class="buka" 	name="appl_tokn" 	value="<?php echo _TOKN;	 	?>"/>
<input type="hidden" class="buka" 	name="targetId"  	value="bukaLoket"/>
<input type="hidden" class="buka" 	name="proses"  		value="buka"/>
<input type="hidden" class="buka" 	name="dump"  		value="0"/>
<table>
	<tr valign="top"> 
		<td colspan="2">
			<p style="padding:6px; font-size:14px;">
				Anda login sebagai <b><?php echo $_SESSION['Name_c']; ?></b><br/>
				Akses dari IP <?php echo _HOST; ?>
			</p>
			<hr/>
		</td>
	</tr>
	<tr valign="top"> 
		<td width="30%" class="form_title">Tanggal Hari Ini</td>
		<td width="70%">:
			<input readonly type="text" name="tanggal" size="15" value="<?php echo date('d-m-Y'); ?>" <?php echo $stsLoket; ?>/>
		</td>
	</tr>        
	<tr>
		<td></td>
		<td id="bukaLoket" align="center">
			<input type="button" value="Proses" onclick="buka('buka')" <?php echo $stsLoket; ?> />
		</td>
	</tr>
</table>
<?php
	}
?>
