<?php
    if($erno) die();
    $kar_id = _USER;
    if(strlen($dkd_kd) == 4){
        $dkd_kd = $_SESSION['Kota_c'].$dkd_kd;
    }
    
    /** koneksi ke database */
    $db = false;
    try {
        $db = new PDO($PSPDO[0],$PSPDO[1],$PSPDO[2]);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $err){
        $mess = $err->getMessage();
        errorLog::errorDB(array($mess));
        $mess = "Mungkin telah terjadi kesalahan pada database server, sehingga koneksi tidak bisa dilakukan.";
        $klas = "error";
    }
    
    switch($proses){
        case 'gantiWM':
            if($db){
                try {
                    $db->beginTransaction();
                    $st0 = 0;
                    $que = "UPDATE tm_pelanggan SET kps_kode=".$kps_kode.",kar_id='".$kar_id."',remark_id='"._TOKN."' WHERE pel_no='".$pel_no."' AND kps_kode=0";
                    $st0 = $db->exec($que);

                    if($st0>0){
                        errorLog::logDB(array($que));
                        $mess = "Perubahan data pelanggan: ".$pel_no." telah di simpan.";
                        $klas = "success";
                    }
                    else{
                        $mess = "Tidak ada perubahan data pelanggan: ".$pel_no;
                        $klas = "error";
                    }
                    $db->commit();
                    //$db->rollBack();
                }
                catch (PDOException $err){
                    $mess = $err->getMessage();
                    errorLog::errorDB(array($mess));
                    errorLog::logDB(array($que));
                    $mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses update SL: ".$pel_no." tidak bisa dilakukan.";
                    $klas = "error";
                }
            }
            break;
        default:
            $mess = "Mungkin telah terjadi kesalahan pada prosedur manual, sehingga tidak ada proses yang bisa dijalankan.";
            $klas = "notice";
    }
    errorLog::logMess(array($mess));
    echo "<div class='".$klas."'>".$mess."</div>";

