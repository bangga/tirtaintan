<?php
	if($erno) die();
	$formId   = getToken();
	$formDump = 0;

	/* koneksi database */
	/* link : link baca */
	$link 	= mysql_connect($DHOST,$DUSER,$DPASS) or die(errorLog::errorDie(array(mysql_error())));
	mysql_select_db($DNAME,$link) or die(errorLog::errorDie(array(mysql_error())));

	/* inquiry status */
	if($kps_kode==17 || $kps_kode==21) $filter = "WHERE kps_kode=".$kps_kode." OR kps_kode=0";
	else $filter = "WHERE kps_kode=".$kps_kode;
	try{
		$que3 = "SELECT kps_kode,UPPER(kps_ket) AS kps_ket FROM tr_kondisi_ps ".$filter." ORDER BY kps_kode";
		if(!$res3 = mysql_query($que3,$link)){
			throw new Exception("Terjadi kesalahan pada sistem database<br/>Nomor Tiket : ".substr(_TOKN,-4));
		}
		else{
			while($row3 = mysql_fetch_array($res3)){
				$data3[] = array("kps_kode"=>$row3['kps_kode'],"kps_ket"=>$row3['kps_ket']);
			}
			$mess = false;
		}
	}
	catch (Exception $e){
		errorLog::errorDB(array($que3));
		$mess = $e->getMessage();
		$erno = false;
	}
	$parm3 = array("class"=>"simpan","id"=>"form-9","name"=>"kps_kode","selected"=>$kps_kode);

	/* retrieve data survey */
	$checked        = "";
	$meterisasi     = 0;
	$stan_pasang    = "";
	$readonly       = "readonly";
	$disabled       = "disabled";
	if($kps_kode==17 OR $kps_kode==21){
		// create a new cURL resource
		$ch 	= curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, 'http://core-data.tirtaintan.co.id/order/pull/'.$pel_no);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$survey	= json_decode(curl_exec($ch));

		// close cURL resource, and free up system resources
		curl_close($ch);

		foreach ($survey->data->$pel_no as $key => $value){
			// retrieve hasil ukuran meter
			if($value->order_kode==2 && $value->result_kode==6 && ($kps_kode==4 || $kps_kode==17 || $kps_kode==21)){
				$um_kode = $value->result_value;
			}
			// retrieve stand pasang
			if($value->order_kode==3 && $value->result_kode==8 && ($kps_kode==4 || $kps_kode==17 || $kps_kode==21)){
				$meterisasi 	= 1;
				$stand_pasang	= $value->result_value;
				$checked	= "checked";
				$readonly 	= "";
				$disabled 	= "";
			}
		}
	}
?>
<div id="<?php echo $formId; ?>" class="peringatan">
<input id="keyProses0" 	type="hidden" value="3"/>
<input id="keyProses1" 	type="hidden" value="0"/>
<input id="tutup" 	type="hidden" value="<?php echo $formId; ?>" />

<div class="pesan form-5">
<div class="span-20 right">[<a title="Tutup jendela ini" onclick="tutup('<?php echo $formId; ?>')">Tutup</a>]</div>
<br/><h3>Form <?php echo _NAME; ?></h3>
<hr/>

<input type="hidden" class="simpan"	name="appl_tokn" 	value="<?php echo _TOKN; 	?>"/>
<input type="hidden" class="simpan"	name="appl_kode" 	value="<?php echo _KODE; 	?>"/>
<input type="hidden" class="simpan"	name="targetUrl" 	value="<?php echo _PROC; 	?>"/>
<input type="hidden" class="simpan"	name="targetId" 	value="targetUpdate"/>
<input type="hidden" class="simpan"	name="proses" 		value="aktivasi"/>
<input type="hidden" class="simpan"	name="dump" 		value="<?php echo $formDump;	?>"/>
<input type="hidden" class="simpan" 	name="pel_no" 		value="<?php echo $pel_no; 	?>"/>
<input type="hidden" class="simpan" 	name="meterisasi"	value="<?php echo $meterisasi;	?>"/>
<input type="hidden" class="simpan"     name="um_kode"		value="<?php echo $um_kode;	?>"/>
<input type="hidden" class="simpan"     name="stand_pasang"     value="<?php echo $stand_pasang;?>"/>
<div>
	<div class="span-9 left border">
		<div class="append-bottom span-3">No Pelanggan</div>
		<div class="append-bottom span-5">: <?php echo $pel_no;		?></div>
		<div class="append-bottom span-3">Kota Pelayanan</div>
		<div class="append-bottom span-5">: <?php echo $kp_ket;		?></div>
		<div class="append-bottom span-3">Nama</div>
		<div class="append-bottom span-5">: <?php echo $pel_nama;	?></div>
		<div class="append-bottom span-3">Alamat</div>
		<div class="append-bottom span-5">: <?php echo $pel_alamat;	?></div>
		<div class="append-bottom span-3">Golongan</div>
		<div class="append-bottom span-5">: <?php echo $gol_kode;	?></div>
		<div class="append-bottom span-3">Rayon</div>
		<div class="append-bottom span-5">: <?php echo $dkd_kd;		?></div>
		<div class="append-bottom span-3">Ukuran Meter</div>
		<div class="append-bottom span-5">: <?php echo $um_ket;		?></div>
		<div class="append-bottom span-3">Meterisasi</div>
		<div class="append-bottom span-5">: <?php echo $met_tgl;	?></div>
		<div class="append-bottom span-3">Status</div>
		<div class="append-bottom span-5">: <?php echo $kps_ket;	?></div>
	</div>
	<div class="span-13 left">
		<div id="targetUpdate" class="span-12"></div>
		<div class="append-bottom span-3">No Pelanggan</div>
		<div class="append-bottom span-7">
			: <?php echo $pel_no; ?>
		</div>
		<div class="append-bottom span-3">Status</div>
		<div class="append-bottom span-7">
			: <?php echo pilihan($data3,$parm3); ?>
		</div>
		<div class="span-3">&nbsp;</div>
		<div class="span-7">&nbsp;
			<input id="form-2" accesskey="S" type="button" value="Simpan" onclick="buka('simpan')"/>
			<input id="jumlahForm" type="hidden" value="2" />
			<input id="aktiveForm" type="hidden" value="0" />
		</div>
	</div>
</div>
</div>
</div>

