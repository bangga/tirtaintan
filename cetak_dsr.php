<?php
	if($erno) die();
	$formId 	= getToken();
	
	/* inquiry data drd */
	$kopel	= explode("_",$kopel);
	$que0 	= "SELECT a.pel_no,SUBSTR(a.pel_nama,1,17) AS pel_nama,a.pel_alamat,a.dkd_kd,a.rek_stanlalu,a.rek_stankini,(a.rek_stankini-a.rek_stanlalu) AS rek_pakai,a.rek_uangair,a.rek_adm,a.rek_meter,a.rek_angsuran,a.rek_total FROM tm_rekening a WHERE a.rek_sts=1 AND a.rek_byr_sts=0 AND a.rek_bln=$rek_bln AND a.rek_thn=$rek_thn AND a.rek_gol='$gol_kode' AND SUBSTR(a.rek_nomor,7,2)='".$kopel[0]."' ORDER BY a.pel_no"; 
	try{
		if(!$res0 = mysql_query($que0,$link)){
			throw new Exception($que0);
		}
		else{
			$i = 0;
			while($row0 = mysql_fetch_array($res0)){
				$data[] = $row0;
				$i++;	
			}
		$mess = false;
		}
	}
	catch (Exception $e){
		errorLog::errorDB(array($que0));
		$mess = $e->getMessage();
	}
?>
<div id="<?php echo $formId; ?>" class="peringatan">
<div class="pesan form-5">
<div class="span-14 right large cetak">
	[<a title="Tutup jendela ini" onclick="tutup('<?php echo $formId; ?>')">Tutup</a>]
	[<a onclick="window.print()">Cetak</a>]
</div>
<h3><?=$appl_owner?> - <?=$kopel[1]?></h3>
<hr/>
<h4><?=_NAME?></h4>
<table width="100%" class="prn_table">
	<tr>
		<td colspan="2">Tanggal Cetak</td>
		<td colspan="6">: <?=$tanggal?></td>
	</tr>
	<tr>
		<td colspan="2">Bulan - Tahun</td>
		<td colspan="6">: <?=$rek_bln?> - <?=$rek_thn?></td>
	</tr>
	<tr>
		<td colspan="2">Golongan</td>
		<td colspan="6">: <?=$gol_kode?></td>
	</tr>
	<tr>
		<td colspan="2">Petugas</td>
		<td colspan="6">: <?=_NAMA?></td>
	</tr>
	<tr class="table_cont_btm">
		<td class="center prn_head">No.</td>
		<td class="center prn_head">No. SL</td>
		<td class="center prn_head">Nama</td>
		<td class="center prn_head">Alamat</td>
		<td class="center prn_head">DKD</td>
		<td class="center prn_head">Stan Lalu<br/>(m3)</td>
		<td class="center prn_head">Stan Kini<br/>(m3)</td>
		<td class="center prn_head">Stan Pakai<br/>(m3)</td>
		<td class="center prn_head">Uang Air<br/>(Rupiah)</td>
		<td class="center prn_head">ADM<br/>(Rupiah)</td>
		<td class="center prn_head">Biaya Meter<br/>(Rupiah)</td>
		<td class="center prn_head">Angsuran<br/>(Rupiah)</td>
		<td class="center prn_head">Total<br/>(Rupiah)</td>
    </tr>
<?php
	for($i=0;$i<count($data);$i++){
		$nomor		= $i+1;
		$row0 	  	= $data[$i];
		$klas 	  	= "table_cell1";
		if(($i%2) == 0){
			$klas = "table_cell2";
		}
		$lembar[$i]		= $row0['rek_lembar'];
		$pakai[$i]		= $row0['rek_stankini']-$row0['rek_stanlalu'];
		$uangair[$i]	= $row0['rek_uangair'];
		$adm[$i]		= $row0['rek_adm'];
		$meter[$i]		= $row0['rek_meter'];
		$angsuran[$i]	= $row0['rek_angsuran'];
		$total[$i]		= $row0['rek_total'];
		$dkd_kd			= $kopel[0].$row0['dkd_kd'];
?>
  <tr class="<?php echo $klas; ?>">
    <td class="right prn_cell"><?php echo number_format($nomor); ?></td>
	<td class="right prn_cell"><?php echo $row0['pel_no']; ?></td>
	<td class="left prn_cell prn_left"><?php echo $row0['pel_nama']; ?></td>
	<td class="left prn_cell prn_left"><?php echo $row0['pel_alamat']; ?></td>
	<td class="center prn_cell"><?php echo $row0['dkd_kd']; ?></td>
    <td class="right prn_cell"><?php echo number_format($row0['rek_stanlalu']); ?></td>
    <td class="right prn_cell"><?php echo number_format($row0['rek_stankini']); ?></td>
    <td class="right prn_cell"><?php echo number_format($pakai[$i]); ?></td>
    <td class="right prn_cell"><?php echo number_format($row0['rek_uangair']); ?></td>
    <td class="right prn_cell"><?php echo number_format($row0['rek_adm']); ?></td>
    <td class="right prn_cell"><?php echo number_format($row0['rek_meter']); ?></td>
    <td class="right prn_cell"><?php echo number_format($row0['rek_angsuran']); ?></td>
    <td class="right prn_cell"><?php echo number_format($row0['rek_total']); ?></td>
  </tr>

<?php
   		}
		if($i>0){
?>
    <tr class="table_cont_btm">
    	<td colspan="7" class="left prn_total">Grand Total :</td>
		<td class="right prn_total"><?php echo number_format(array_sum($pakai)); ?></td>
	 	<td class="right prn_total"><?php echo number_format(array_sum($uangair)); ?></td>
   		<td class="right prn_total"><?php echo number_format(array_sum($adm)); ?></td>
   		<td class="right prn_total"><?php echo number_format(array_sum($meter)); ?></td>
   		<td class="right prn_total"><?php echo number_format(array_sum($angsuran)); ?></td>
   		<td class="right prn_total"><?php echo number_format(array_sum($total)); ?></td>
  </tr>
<?php
		}
?>
</table>
</div>
</div>