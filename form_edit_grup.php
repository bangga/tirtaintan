<?php
	if($erno) die();
	$formId 	= getToken();
	$targetId 	= getToken();
	$errorId 	= getToken();

        $data2[]        = array("grup_sts"=>"1","kp_ket"=>"Unit");
        $data2[]        = array("grup_sts"=>"2","kp_ket"=>"Cabang");
        $data2[]        = array("grup_sts"=>"3","kp_ket"=>"Pusat");
        $parm2          = array("class"=>"simpan","name"=>"grup_sts","selected"=>$grup_sts);
?>
<div id="<?php echo $formId; ?>" class="peringatan">
<input type="hidden" id="keyProses0" 	value="1" />
<input type="hidden" id="tutup" 		value="<?php echo $formId; ?>" />
<div class="pesan span-18">
<div class="span-18 right large">[<a title="Tutup jendela ini" onclick="tutup('<?php echo $formId; ?>')">Tutup</a>]</div>
<h3>Form Edit Grup Pengguna</h3>
<hr/>
<div id="<?php echo $targetId; ?>" class="span-18"></div>
<div class="span-8 left">
	<div class="span-2 prepend-top">Grup Kode</div>
	<div class="span-5 prepend-top">: <?php echo $grup_id; ?></div>
	<div class="span-2 prepend-top">Grup Nama</div>
	<div class="span-5 prepend-top">:
		<input type="text" size="20" class="simpan" name="grup_nama" value="<?php echo $grup_nama; ?>"/>
	</div>
        <div class="span-2 prepend-top">Satuan</div>
        <div class="span-5 prepend-top">
                : <?php echo pilihan($data2,$parm2); ?>
        </div>
	<div class="span-2 prepend-top">&nbsp;</div>
	<div class="span-5 prepend-top">&nbsp;
		<input type="hidden" class="simpan" name="targetId" 	value="<?php echo $targetId;?>"/>
		<input type="hidden" class="simpan" name="errorId" 	value="<?php echo $errorId;	?>"/>
		<input type="hidden" class="simpan" name="targetUrl" 	value="<?php echo _PROC; 	?>"/>
		<input type="hidden" class="simpan" name="appl_kode" 	value="<?php echo _KODE; 	?>"/>
		<input type="hidden" class="simpan" name="grup_id" 	value="<?php echo $grup_id;	?>"/>
		<input type="hidden" class="simpan" name="proses"	value="editGrup"/>
		<input type="hidden" class="simpan" name="dump"		value="0"/>
		<input type="button" class="form_button" value="Simpan" onclick="buka('simpan')"/>
	</div>
</div>
</div>
</div>

