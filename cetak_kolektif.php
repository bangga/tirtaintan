<?php
	if($erno) die();
	$formId 	= getToken();

	$que0 = "SELECT a.* FROM v_dsr a JOIN tm_kolektif b ON(b.pel_no=a.pel_no) WHERE b.kel_kode='$kel_kode' ORDER BY a.pel_no,a.rek_thn ASC,a.rek_bln ASC";
	try{
		if(!$res0 = mysql_query($que0,$link)){
			throw new Exception($que0);
		}
		else{
			while($row0 = mysql_fetch_array($res0)){
				$data[$row0['pel_no']][] = $row0;
			}
			$mess = false;
		}
	}
	catch (Exception $e){
		errorLog::errorDB(array($que0));
		$mess = $e->getMessage();
	}
	
?>
<div id="<?php echo $formId; ?>" class="peringatan">
<input type="hidden" id="keyProses0" value="2"/>
<input type="hidden" id="tutup" value="<?php echo $formId; ?>"/>
<div class="pesan form-5 prepend-top">
<div class="span-14 right large cetak">
	[<a title="Tutup jendela ini" onclick="tutup('<?php echo $formId; ?>')">Tutup</a>]
	[<a onclick="window.print()">Cetak</a>]
</div>
<table width="100%" class="prn_table">
  <tr>
	<td colspan="4" class="center"><h3>Info Tagihan Kolektif</h3></td>
  </tr>
  <tr class="append-3 prepend-3">
    <td width="20%">Kolektor</td>
    <td width="40%">: [<?php echo $kel_nama; ?>] <?php echo $kolektor; ?></td>
    <td width="15%">&nbsp;</td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td>: <?php echo $kel_ket; ?></td>
    <td></td>
    <td></td>
  </tr>
</table>
<hr/>
<table width="100%" class="prn_table">
  <tr class="table_cont_btm">
    <td colspan="2" class="prn_cell">Bulan / Tahun </td>
    <td colspan="3" class="center prn_cell">Stand Meter </td>
    <td colspan="4" class="center prn_cell">Rincian Biaya </td>
    <td class="center prn_cell">Total</td>
  </tr>
  <tr class="table_cont_btm center">
  	<td colspan="2" class="prn_cell"></td>
    <td class="center prn_cell">Lalu</td>
    <td class="center prn_cell">Kini</td>
    <td class="center prn_cell">Pakai</td>
    <td class="center prn_cell">Air</td>
    <td class="center prn_cell">Angsuran</td>
    <td class="center prn_cell">Beban Tetap</td>
    <td class="center prn_cell">Denda</td>
	<td class="center prn_cell">&nbsp;</td>
    </tr>
<?php
	if(count($data)>0){
		$level1_val 	= $data;
		$level1_key 	= array_keys($level1_val);
		$nomer2			= 0;
		/* order by level 1 pelanggan */
		for($i=0;$i<count($level1_val);$i++){
			$pel_no		= $level1_key[$i];
			$pel_nama	= $level1_val[$pel_no][0]['pel_nama'];
			$pel_alamat	= $level1_val[$pel_no][0]['pel_alamat'];
			$nomer1		= ($i+1).". ".$pel_no;
?>
	<tr>
		<th colspan="3" align="left" class="prn_cell prn_left"><?php echo $nomer1;		?></th>
		<th colspan="3" align="left" class="prn_cell prn_left"><?php echo $pel_nama;	?></th>
		<th colspan="5" align="left" class="prn_cell prn_left"><?php echo $pel_alamat;	?></th>
	</tr>
<?php
			$level2_val		= $level1_val[$level1_key[$i]];
			$level2_key		= array_keys($level2_val);
			/* order by level 2 rincian tunggakan */
			for($k=0;$k<count($level2_val);$k++){
				$nomer2++;
				$nilai	= $level2_val[$level2_key[$k]];
				$klas 	  = "table_cell1";
				if(($k%2) == 0){
					$klas = "table_cell2";
				}
				$kunci	= array_keys($nilai);
				for($m=0;$m<count($kunci);$m++){
					$$kunci[$m] = $nilai[$kunci[$m]];
				}
				$pemakaian				= $rek_stankini - $rek_stanlalu;
				$beban_tetap			= $rek_adm + $rek_meter;
				$total					= $rek_total + $rek_denda;
				$l1_pakai[$pel_no][]	= $pemakaian;
				$l1_uangair[$pel_no][]	= $rek_uangair;
				$l1_beban[$pel_no][]	= $beban_tetap;
				$l1_angsuran[$pel_no][]	= $rek_angsuran;
				$l1_denda[$pel_no][]	= $rek_denda;
				$l1_total[$pel_no][]	= $total;
				
				$l0_pakai[]		= $pemakaian;
				$l0_uangair[]	= $rek_uangair;
				$l0_beban[]		= $beban_tetap;
				$l0_angsuran[]	= $rek_angsuran;
				$l0_denda[]		= $rek_denda;
				$l0_total[]		= $total;
?>
	<tr class="<?php echo $klas; ?>">
		<td class="right prn_cell"><?php echo $nomer2; ?>.</td>
		<td class="right prn_cell"><?php echo $rek_bln." - ".$rek_thn;?></td>
		<td class="right prn_cell"><?php echo number_format($rek_stanlalu); ?></td>
		<td class="right prn_cell"><?php echo number_format($rek_stankini); ?></td>
		<td class="right prn_cell"><?php echo number_format($pemakaian); 	?></td>
		<td class="right prn_cell"><?php echo number_format($rek_uangair); 	?></td>
		<td class="right prn_cell"><?php echo number_format($rek_angsuran); ?></td>
		<td class="right prn_cell"><?php echo number_format($beban_tetap); 	?></td>
		<td class="right prn_cell"><?php echo number_format($rek_denda); 	?></td>
		<td class="right prn_cell"><?php echo number_format($total); 		?></td>
	</tr>
<?php
			}
?>
    <tr class="table_cont_btm">
    	<td colspan="4" class="table_cont_btm prn_cell">Total :</td>
	 	<td class="right prn_cell"><?php echo number_format(array_sum($l1_pakai[$pel_no])); 	?></td>
	 	<td class="right prn_cell"><?php echo number_format(array_sum($l1_uangair[$pel_no])); 	?></td>
		<td class="right prn_cell"><?php echo number_format(array_sum($l1_angsuran[$pel_no])); 	?></td>
	 	<td class="right prn_cell"><?php echo number_format(array_sum($l1_beban[$pel_no])); 	?></td>
	 	<td class="right prn_cell"><?php echo number_format(array_sum($l1_denda[$pel_no])); 	?></td>
	 	<td class="right prn_cell"><?php echo number_format(array_sum($l1_total[$pel_no])); 	?></td>
	</tr>
<?php
   		}
?>
    <tr class="table_cont_btm">
    	<td colspan="4" class="table_cont_btm prn_cell">Grand Total :</td>
	 	<td class="right prn_cell"><?php echo number_format(array_sum($l0_pakai));	 	?></td>
	 	<td class="right prn_cell"><?php echo number_format(array_sum($l0_uangair)); 	?></td>
		<td class="right prn_cell"><?php echo number_format(array_sum($l0_angsuran)); 	?></td>
	 	<td class="right prn_cell"><?php echo number_format(array_sum($l0_beban)); 		?></td>
	 	<td class="right prn_cell"><?php echo number_format(array_sum($l0_denda)); 		?></td>
	 	<td class="right prn_cell"><?php echo number_format(array_sum($l0_total)); 		?></td>
	</tr>
<?php
	}
?>
</table>
</div>
</div>
