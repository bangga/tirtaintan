<?php
	if($erno) die();
	unset($mess);
	
	switch($proses){
		case "cetak":
			try{
				$wsdl_url 	= "http://"._PRIN."/printClient/printServer.wsdl";
				$client   	= new SoapClient($wsdl_url, array('cache_wsdl' => WSDL_CACHE_NONE) );
				$cetak 		= true;
			}
			catch (Exception $e){
				$mess		= "Perangkat pencetak belum tersedia.";
				errorLog::errorDB(array($mess));
				$cetak 		= false;
			}
			
			if($cetak){
				try {
					$stringFile	  = _TOKN.$rek_nomor.".txt";
					$openFile 	  = fopen("_data/".$stringFile, 'w');
					fwrite($openFile, base64_decode($stringCetak));
					fclose($openFile);
					$client->cetak($stringCetak,$stringFile);
					$mess	= "Proses cetak copy resi : ".$byr_no.$rek_nomor." telah dilakukan.";
				}
				catch (Exception $err) {
					$mess	= "Proses cetak copy resi : ".$byr_no.$rek_nomor." gagal dilakukan.";
				}
			}
			errorLog::logMess(array($mess));
			echo "$mess";
			break;
		case "rinci":
			try{
                                if(_USER=='admin'){
					$que0 	= "SELECT a.*,IF((rek_bln=".$rek_bln." AND rek_thn=".$rek_thn."),1,0) AS pilih FROM v_lpp a WHERE a.byr_sts=1 AND a.pel_no='$pel_no' ORDER BY a.rek_thn ASC,a.rek_bln ASC";
                                 
                                }
				else if(strlen($pel_no)==6){
					$que0 	= "SELECT a.*,IF((rek_bln=".$rek_bln." AND rek_thn=".$rek_thn."),1,0) AS pilih FROM v_lpp a WHERE a.kar_id='"._USER."' AND a.pel_no='$pel_no' AND DATE(a.byr_tgl)=CURDATE() ORDER BY a.rek_thn ASC,a.rek_bln ASC";
				}
				else{
					$pel_no	= substr($pel_no,0,2).".".substr($pel_no,2,2).".".substr($pel_no,4,3).".".substr($pel_no,7,3);
					$que0 	= "SELECT a.*,IF((rek_bln=".$rek_bln." AND rek_thn=".$rek_thn."),1,0) AS pilih FROM v_lpp a WHERE a.kar_id='"._USER."' AND a.pel_no='$pel_no' AND DATE(a.byr_tgl)=CURDATE() ORDER BY a.rek_thn ASC,a.rek_bln ASC";
				}
				if(!$res0 = mysql_query($que0,$link)){
					throw new Exception(mysql_error($link));
				}
				else{
					while($row0 = mysql_fetch_array($res0)){
						$data[] 		= $row0;
						$grandTotal[]	= $row0['rek_total'] + $row0['rek_denda'] + $row0['rek_materai'];
						$pel_nama		= $row0['pel_nama'];
						$pel_alamat		= $row0['pel_alamat'];
						$golongan		= $row0['gol_ket'];
						if($row0['pilih']==1){
							$byr_no 		= $row0['byr_no'];
							$rek_nomor 		= $row0['rek_nomor'];
						}
					}
					$mess = false;
				}
			}
			catch (Exception $e){
				errorLog::errorDB(array($que0));
				$mess = "Terjadi kesalahan pada sistem<br/>Nomor Tiket : ".substr(_TOKN,-4);
			}
			if(!$erno) mysql_close($link);
?>
<h2 class="cetak"><?php echo _NAME; ?></h2><hr class="cetak" />
<input id="<?php echo $errorId; ?>" type="hidden" value="<?=$mess?>"/>
<input type="hidden" id="norefresh" 	value="1" />
<input type="hidden" id="keyProses1" 	value="F" />
<input type="hidden" id="jumlahForm" 	value="1" />
<input type="hidden" id="aktiveForm" 	value="0" />
<input type="hidden" class="valCopy" 			name="appl_tokn"	value="<?php echo _TOKN;	 	?>"/>
<input type="hidden" class="kembali valCopy" 	name="appl_kode"	value="<?php echo _KODE; 		?>"/>
<input type="hidden" class="kembali valCopy" 	name="appl_name"	value="<?php echo _NAME; 		?>"/>
<input type="hidden" class="kembali valCopy" 	name="appl_file"	value="<?php echo _FILE; 		?>"/>
<input type="hidden" class="kembali valCopy" 	name="appl_proc"	value="<?php echo _PROC; 		?>"/>
<input type="hidden" class="kembali" 			name="targetUrl"	value="<?php echo _FILE; 		?>"/>
<input type="hidden" class="kembali" 			name="targetId"		value="content"/>
<input type="hidden" class="valCopy" 			name="targetUrl" 	value="<?php echo _PROC;		?>"/>
<input type="hidden" class="valCopy" 			name="byr_no" 		value="<?php echo $byr_no;		?>"/>
<input type="hidden" class="valCopy" 			name="rek_nomor" 	value="<?php echo $rek_nomor;	?>"/>
<input type="hidden" class="valCopy" 			name="errorId"		value="<?php echo getToken(); 	?>"/>
<input type="hidden" class="valCopy" 			name="targetId"		value="targetId"/>
<input type="hidden" class="valCopy" 			name="proses"		value="cetak"/>
<input type="hidden" class="valCopy" 			name="dump"			value="0" />
<table class="table_info">
	<tr class="table_validator">
		<td colspan="2">Nomor SL</td>
		<td colspan="5">:<?php echo $pel_no; 	?></td>
		<td colspan="1">Golongan</td>
		<td colspan="4">:<?php echo $golongan; 	?></td>
	</tr>
	<tr class="table_validator">
		<td colspan="2">Nama</td>
		<td colspan="5">:<?php echo $pel_nama; 	?></td>
		<td colspan="1">Alamat</td>
		<td colspan="4">:<?php echo $pel_alamat;?></td>
	</tr>
	<tr>
		<td colspan="8">&nbsp;</td>
	</tr>
	<tr class="table_head"> 
		<td colspan="8" align="left">
			<input type="button" value="Kembali" onclick="buka('kembali')"/>
		</td>
		<td align="left">Jumlah Rekening :<?=number_format(count($data))?></td>
		<td class="right">Grand Total :</td>				
		<td class="right" valign="center"><b>
<?php
	if(count($data)>0){
		echo number_format(array_sum($grandTotal));
	}
	else{
		echo 0;
	} 
?>
		</b></td>
		<td align="center"></td>				
	</tr>
	<tr class="table_cont_btm center">
		<td rowspan="2">No.</td>
		<td rowspan="2">Bulan / Tahun</td>
		<td colspan="3" class="center">Stand Meter</td>
		<td colspan="5" class="center">Rincian Biaya</td>
		<td rowspan="2" class="center">Total</td>
		<td rowspan="2"></td>
	</tr>
	<tr class="table_cont_btm center">
		<td class="center">Lalu</td>
		<td class="center">Kini</td>
		<td class="center">Pakai</td>
		<td class="center">Air</td>
		<td class="center">Angsuran</td>
		<td class="center">Beban Tetap</td>
		<td class="center">Denda</td>
		<td class="center">Materai</td>
	</tr>
<?php
			if(count($data)>0){
				for($i=0;$i<count($data);$i++){
					$class_nya 		= "table_cell1";
					if ($i%2==0){
						$class_nya 	= "table_cell2";
					}
					/** getParam 
						memindahkan semua nilai dalam array POST ke dalam
						variabel yang bersesuaian dengan masih kunci array
					*/
					$nilai	= $data[$i];
					$konci	= array_keys($nilai);
					for($j=0;$j<count($konci);$j++){
						$$konci[$j]	= $nilai[$konci[$j]];
					}
					/* getParam **/
					$form = false;
					if(isset($byr_no) and isset($rek_nomor)){
						$form = true;
					}
					if($form){
						switch($byr_loket){
							case "N":
								$noresi = "O";
								break;
							case "T":
								$noresi = "U";
								break;
							case "D":
								$noresi = "E";
								break;
							default:
								$noresi = "C";
						}
						$rek_bayar = $grandTotal[$i];l
?>
	<tr class="<?=$class_nya?>">
		<td class="right"><?=($i+1)?></td>
		<td class="right"><?=$bulan[$rek_bln]?><?=$rek_thn?></td>
		<td class="right"><?=number_format($rek_stanlalu)?></td>
		<td class="right"><?=number_format($rek_stankini)?></td>
		<td class="right"><?=number_format($pemakaian)?></td>
		<td class="right"><?=number_format($rek_uangair)?></td>
		<td class="right"><?=number_format($rek_angsuran)?></td>
		<td class="right"><?=number_format($beban_tetap)?></td>
		<td class="right"><?=number_format($rek_denda)?></td>
		<td class="right"><?=number_format($rek_materai)?></td>
		<td class="right"><?=number_format($grandTotal[$i])?></td>
		<td></td>
	</tr>
<?
						if($pilih){
							// line untuk ff continous paper
							$stringCetak  = chr(27).chr(67).chr(3);
							// enable paper out sensor
							$stringCetak .= chr(27).chr(57);
							// draft mode
							$stringCetak .= chr(27).chr(120).chr(48);
							// line spacing x/72
							$stringCetak .= chr(27).chr(65).chr(12);
							$stringCetak .= chr(10).chr(10).chr(10).chr(10);
							$stringCetak .= "REK.BULAN   : ".printRight($bulan[$rek_bln]." ".$rek_thn,16).		"  REK.BULAN   : ".$bulan[$rek_bln]." ".$rek_thn.chr(10);
							$stringCetak .= "NO.LANGG    : ".printRight($pel_no,16).							"  NO.LANGG    : ".printLeft($pel_no,10).						" TARIF       : ".printRight($gol_kode,8).chr(10);
							$stringCetak .= "TARIF       : ".printRight($gol_kode,16).							"  NAMA        : ".$pel_nama.chr(10);
							$stringCetak .= "NAMA        : ".printRight(substr($pel_nama,0,16),16).				"  ALAMAT      : ".$pel_alamat.chr(10);
							$stringCetak .= "ALAMAT      : ".printRight(substr($pel_alamat,0,16),16).			"  HARGA AIR   : ".printRight(number_format($rek_uangair),10).	" STAND AKHIR : ".printRight(number_format($rek_stankini),8).chr(10);
							$stringCetak .= "STAND AKHIR : ".printRight(number_format($rek_stankini),16).		"  BIAYA BEBAN : ".printRight(number_format($beban_tetap),10).	" STAND AWAL  : ".printRight(number_format($rek_stanlalu),8).chr(10);
							$stringCetak .= "STAND AWAL  : ".printRight(number_format($rek_stanlalu),16).		"  CICILAN BP  : ".printRight(number_format($rek_angsuran),10).	" JML.PAKAI   : ".printRight(number_format($pemakaian),8).chr(10);
							$stringCetak .= "JML.PAKAI   : ".printRight(number_format($pemakaian),16).			"  DENDA       : ".printRight(number_format($rek_denda),10).chr(10);
							$stringCetak .= "HARGA AIR   : ".printRight(number_format($rek_uangair),16).		"  JML.TAGIHAN : ".printRight(number_format($rek_bayar),10).chr(10);
							$stringCetak .= "BIAYA BEBAN : ".printRight(number_format($beban_tetap),16).		"  TERBILANG   : ".strtoupper(substr((n2c($rek_bayar,"Rupiah")),0,33)).chr(10);
							$stringCetak .= "CICILAN BP  : ".printRight(number_format($rek_angsuran),16).		"                ".strtoupper(substr((n2c($rek_bayar,"Rupiah")),33,33)).chr(10);
							$stringCetak .= "DENDA       : ".printRight(number_format($rek_denda),16).			"                ".strtoupper(substr((n2c($rek_bayar,"Rupiah")),66,33)).chr(10);
							$stringCetak .= "JML.TAGIHAN : ".printRight(number_format($rek_bayar),16).			"                ".strtoupper(substr((n2c($rek_bayar,"Rupiah")),99,33)).chr(10);
							$stringCetak .= "REFERENSI   : ".$byr_no.											"  REFERENSI   : ".$loket.$byr_no.chr(10);
							$stringCetak .= "KASIR       : ".printRight($noresi."-".$kar_id,16).				"  KASIR       : ".$noresi."-".$kar_id.chr(10);
							if($rek_materai>0){
								$stringCetak .= printRight("Mat. ".$rek_materai,77);
							}
							$stringCetak .= chr(12);
						}
					}
				}
			}
			else{
?>
	<tr><td class="notice" colspan="12">Transaksi pembayaran tidak ditemukan.</td></tr>
<?php
			}
?>					   				   
	<tr class="table_head">
		<td colspan="12" id="targetId">
			<input type="hidden" class="valCopy" name="stringCetak" value="<?php echo base64_encode($stringCetak); ?>" />
			<input id="form-1" type="button" value="Cetak" onclick="buka('valCopy')" /> 
		</td>				
	</tr>
</table>
<?php
			break;
		default:
			$que1	= "SELECT IFNULL(MAX(tr_sts),0) AS tr_sts FROM tr_trans_log WHERE kar_id='"._USER."' AND DATE(tr_tgl)=CURDATE() AND tr_sts<5";
			$res1 	= mysql_query($que1,$link);
			$row1 	= mysql_fetch_array($res1);
			$tr_sts	= abs($row1['tr_sts']);
			switch($tr_sts){
				case 3:
					$status = "";
					break;
				case 4:
					$status = "disabled";
					$mess	= "Loket telah ditutup.";
					break;
				default:
					$status = "disabled";
					$mess	= "Loket belum dibuka.";
			}
			for($i=1;$i<=12;$i++){
				$data1[] = array("rek_bln"=>$i,"bln_nama"=>$bulan[$i]);
			}
			$rek_bln= date('n');
			$rek_thn = date('Y');
			if($rek_bln>1){
				$rek_bln--;
				
			}
			else{
				$rek_bln = 12;
				$rek_thn--;
			}
			$parm1	= array("class"=>"buka","id"=>"form-2","name"=>"rek_bln","selected"=>$rek_bln);
?>
<h2 class="cetak"><?php echo _NAME; ?></h2><hr class="cetak" />
<input id="<?php echo $errorId; ?>" type="hidden" value="<?=$mess?>"/>
<input type="hidden" id="norefresh" 	value="1" />
<input type="hidden" id="keyProses1" 	value="C" />
<input type="hidden" id="jumlahForm" 	value="4" />
<input type="hidden" id="aktiveForm" 	value="0" />
<input type="hidden" class="buka" 	name="appl_tokn"	value="<?php echo getToken();	?>"/>
<input type="hidden" class="buka" 	name="appl_kode"	value="<?php echo _KODE; 		?>"/>
<input type="hidden" class="buka" 	name="appl_name"	value="<?php echo _NAME; 		?>"/>
<input type="hidden" class="buka" 	name="appl_file"	value="<?php echo _FILE; 		?>"/>
<input type="hidden" class="buka" 	name="appl_proc"	value="<?php echo _PROC; 		?>"/>
<input type="hidden" class="buka" 	name="errorId"		value="<?php echo getToken();	?>"/>
<input type="hidden" class="buka" 	name="targetUrl" 	value="<?php echo _FILE; 		?>"/>
<input type="hidden" class="buka" 	name="targetId"		value="content"/>
<input type="hidden" class="buka" 	name="proses"	 	value="rinci"/>
<div class="span-14">
	<div class="span-4">&nbsp;</div>
	<div class="span-4">Nomor Pelanggan</div>
	<div class="span-5">:
		<input <?=$status?> id="form-1" type="text" class="buka sl" name="pel_no" size="13" style="font-size:15pt; font-family:courier;" maxlength="10" onmouseover="$(this.id).focus()" />
	</div>
	<div class="span-4">&nbsp;</div>
	<div class="span-4">Bulan - Tahun</div>
	<div class="span-5">:
		<?php echo pilihan($data1,$parm1); ?>
		<input type="text" id="form-3" class="buka" name="rek_thn" size="4" maxlength="4" value="<?php echo $rek_thn; ?>"/>
	</div>
	<div class="span-12">&nbsp;</div>
	<div class="span-12 right">
		<input <?=$status?> type="Button" id="form-4" value="Cek Rekening" onclick="buka('buka')"/>
	</div>
</div>
<?php
	}
?>
