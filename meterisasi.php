<?php
    if($erno) die();
    $kar_id = _USER;
    if(strlen($dkd_kd) == 4){
        $dkd_kd = $_SESSION['Kota_c'].$dkd_kd;
    }
    
    /** koneksi ke database */
    $db        = false;
    try {
        $db     = new PDO($PSPDO[0],$PSPDO[1],$PSPDO[2]);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $err){
        $mess = $err->getMessage();
        errorLog::errorDB(array($mess));
        $mess = "Mungkin telah terjadi kesalahan pada database server, sehingga koneksi tidak bisa dilakukan.";
        $klas = "error";
    }
    
    switch($proses){
        case 'meterisasi':
            if($db){
                try {
                    $db->beginTransaction();
                    $st0 = 0;
                    if($kps_kode==0 && $meterisasi==1){
                        $que = "UPDATE tm_pelanggan SET met_tgl=NOW(),met_stdbaru=".$stand_pasang.",kar_id='".$kar_id."',remark_id='"._TOKN."' WHERE pel_no='".$pel_no."' AND kps_kode=0";
                        if($db->exec($que)){
                            $st0++;
                            errorLog::logDB(array($que));
                        }

                        $que = "INSERT INTO tm_meterisasi(remark_tgl,pel_no,um_kode,met_stdbaru,remark_um_kode,remark_met_stdbaru,kar_id) VALUES(NOW(),'".$pel_no."',1,".$stand_angkat.",1,".$stand_pasang.",'".$kar_id."')";
                        if($db->exec($que)){
                            $st0++;
                            errorLog::logDB(array($que));
                        }
                    }
                    
                    if($st0==2){
                        //$db->rollBack();
                        $db->commit();
                        $mess = "Perubahan data pelanggan: ".$pel_no." telah di simpan.";
                        $klas = "success";
                    }
                    else{
                        $db->rollBack();
                        $mess = "Tidak ada perubahan data pelanggan: ".$pel_no;
                        $klas = "success";
                    }
                }
                catch (PDOException $err){
                    $mess = $err->getMessage();
                    errorLog::errorDB(array($mess));
                    errorLog::logDB(array($que));
                    $mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses update SL: ".$pel_no." tidak bisa dilakukan.";
                    $klas = "error";
                }
            }
            break;
        default:
            $mess = "Mungkin telah terjadi kesalahan pada prosedur manual, sehingga tidak ada proses yang bisa dijalankan.";
            $klas = "notice";
    }
    errorLog::logMess(array($mess));
    echo "<div class='".$klas."'>".$mess."</div>";

