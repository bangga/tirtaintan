<?php
	if($erno) die();
	$formId 	= getToken();
	$kopel		= $_SESSION['Kota_c']."_".$_SESSION['kp_ket'];
	if($_SESSION['Group_c']=='000'){
		$filtered = '';
	}
	else if($_SESSION['c_group']=='00'){
		$filtered = '';
	}
	else if($_SESSION['grup_sts']=='2'){
		$filtered = "WHERE cab_kode='".$_SESSION['c_group']."'";
	}
	else{
		$filtered = "WHERE kp_kode='".$_SESSION['Kota_c']."'";
	}
	
	/* inquiry kota pelayanan */
	try{
		$que2 = "SELECT CONCAT(kp_kode,'_',kp_ket) AS kopel,CONCAT('[',kp_kode,'] ',kp_ket) AS kp_nama FROM tr_kota_pelayanan ".$filtered." ORDER BY kp_ket ASC";
		if(!$res2 = mysql_query($que2,$link)){
			throw new Exception("Terjadi kesalahan pada sistem database<br/>Nomor Tiket : ".substr(_TOKN,-4));
		}
		else{
			while($row2 = mysql_fetch_array($res2)){
				$data2[] = array("kopel"=>$row2['kopel'],"kp_ket"=>$row2['kp_nama']);
			}
			//$data2[] = array("kopel"=>"99_Seluruh Cabang","kp_ket"=>"[99] Seluruh Cabang");
			$mess = false;
		}
	}
	catch (Exception $e){
		errorLog::errorDB(array($que2));
		$mess = $e->getMessage();
		$erno = false;
	}
	$parm2 		= array("class"=>"cetak","name"=>"kopel","selected"=>$kopel);
	
	/* inquiry golongan */
	$rek_bln = date('n');
	$rek_thn = date('Y');
	if($rek_bln==1){
		$rek_bln = 12;
		$rek_thn = $rek_thn-1;
	}
	else{
		$rek_bln = $rek_bln-1;
	}
	switch(_KODE){
		case '050901':
			$disabled	= "disabled";
			$data3[]	= array("gol_kode"=>"1","gol_ket"=>"Abnormal Atas");
			$data3[]	= array("gol_kode"=>"2","gol_ket"=>"Abnormal Bawah");
			$data3[]	= array("gol_kode"=>"3","gol_ket"=>"Abnormal Negatif");
			$data3[]	= array("gol_kode"=>"4","gol_ket"=>"Pemakaian Nol");
			$data3[]	= array("gol_kode"=>"5","gol_ket"=>"Belum Diisi");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","disabled"=>"disabled","selected"=>date('n'));
			$parm2 		= array("class"=>"cetak","name"=>"kopel","disabled"=>"disabled","selected"=>$kopel);
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>1);
			$rek_thn	= date('Y');
			break;
		case "060201":
			$disabled	= "disabled";
			$data3[]	= array("gol_kode"=>NULL,"gol_ket"=>"-");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>$rek_bln,"disabled"=>"disabled");
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			try{
				$que3 = "SELECT gol_kode,CONCAT('[',gol_kode,'] ',gol_ket) AS gol_ket FROM tr_gol ORDER BY gol_kode ASC";
				if(!$res3 = mysql_query($que3,$link)){
					throw new Exception("Terjadi kesalahan pada sistem database<br/>Nomor Tiket : ".substr(_TOKN,-4));
				}
				else{
					while($row3 = mysql_fetch_array($res3)){
						$data3[] = array("gol_kode"=>$row3['gol_kode'],"gol_ket"=>$row3['gol_ket']);
					}
					$mess = false;
				}
			}
			catch (Exception $e){
				errorLog::errorDB(array($que3));
				$mess = $e->getMessage();
				$erno = false;
			}
			break;
		case "060203":
			$data3[]	= array("gol_kode"=>NULL,"gol_ket"=>"-");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>$rek_bln);
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			try{
				$que3 = "SELECT gol_kode,CONCAT('[',gol_kode,'] ',gol_ket) AS gol_ket FROM tr_gol ORDER BY gol_kode ASC";
				if(!$res3 = mysql_query($que3,$link)){
					throw new Exception("Terjadi kesalahan pada sistem database<br/>Nomor Tiket : ".substr(_TOKN,-4));
				}
				else{
					while($row3 = mysql_fetch_array($res3)){
						$data3[] = array("gol_kode"=>$row3['gol_kode'],"gol_ket"=>$row3['gol_ket']);
					}
					$mess = false;
				}
			}
			catch (Exception $e){
				errorLog::errorDB(array($que3));
				$mess = $e->getMessage();
				$erno = false;
			}
			break;
		// rekapitulasi drd akhir
		case "060102":
			$data3[]	= array("gol_kode"=>"CB","gol_ket"=>"Cabang");
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>$rek_bln);
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			break;
		// rinci reduksi
		case "060105":
			$data3[]	= array("gol_kode"=>"-","gol_ket"=>"-");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>$rek_bln);
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL,"disabled"=>"disabled");
			break;
		case "060106":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>$rek_bln);
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			break;
		// rekapitulasi drd awal
		case "060107":
			$data2[] 	= array("kopel"=>"99_Seluruh Cabang","kp_ket"=>"[99] Seluruh Cabang");
			$data3[]	= array("gol_kode"=>"CB","gol_ket"=>"Cabang");
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>$rek_bln);
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			break;
		case "060109":
			$rek_thn	= date('Y');
			$data3[]	= array("gol_kode"=>"1","gol_ket"=>"BP");
			$data3[]	= array("gol_kode"=>"2","gol_ket"=>"PKPT");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>1);
			break;
		case "060110":
			$rek_thn	= date('Y');
			$data3[]	= array("gol_kode"=>"1","gol_ket"=>"BP");
			$data3[]	= array("gol_kode"=>"2","gol_ket"=>"PKPT");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>1);
			break;
		// Rekapitulasi DSR
		case "060204":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>$rek_bln);
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			break;
                // Rincian DSR
                case "060205":
                        $parm1          = array("class"=>"cetak","name"=>"rek_bln","selected"=>$rek_bln);
			$parm2          = array("class"=>"cetak","name"=>"kopel","disabled"=>"disabled","selected"=>$kopel);
                        $parm3          = array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			try{
				$que3 = "SELECT dkd_kd AS gol_kode,CONCAT('[',dkd_kd,'] ',dkd_jalan) AS gol_ket FROM tr_dkd WHERE SUBSTR(dkd_kd,1,2)='".$_SESSION['Kota_c']."' ORDER BY dkd_kd ASC";
				if(!$res3 = mysql_query($que3,$link)){
					throw new Exception("Terjadi kesalahan pada sistem database<br/>Nomor Tiket : ".substr(_TOKN,-4));
				}
				else{
					while($row3 = mysql_fetch_array($res3)){
						$data3[] = array("gol_kode"=>$row3['gol_kode'],"gol_ket"=>$row3['gol_ket']);
					}
					$mess = false;
				}
			}
			catch (Exception $e){
				errorLog::errorDB(array($que3));
				$mess = $e->getMessage();
				$erno = false;
			}
                        break;
		case "060901":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			$rek_thn	= date('Y');
			break;
		case "060902":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			$rek_thn	= date('Y');
			break;
		case "060905":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			$rek_thn	= date('Y');
			break;
		// rincian mutasi golongan
		case "060906":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			$rek_thn	= date('Y');
			break;
		case "060907":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			$rek_thn	= date('Y');
			break;
		// rincian siap sambung
		case "060908":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'),"disabled"=>"disabled");
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL,"disabled"=>"disabled");
			$rek_thn	= date('Y');
			$disabled	= "disabled";
			break;
		case "060909":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL,"disabled"=>"disabled");
			break;
		// rincian siap survey
		case "060910":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'),"disabled"=>"disabled");
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL,"disabled"=>"disabled");
			$rek_thn	= date('Y');
			$disabled	= "disabled";
			break;
                // rincian mutasi nama
                case "060911":
			$data2[] 	= array("kopel"=>"99_Seluruh Cabang","kp_ket"=>"[99] Seluruh Cabang");
                        $data3[]        = array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
                        $data3[]        = array("gol_kode"=>"KG","gol_ket"=>"Golongan");
                        $parm1          = array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
                        $parm3          = array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
                        $rek_thn        = date('Y');
                        break;
		// rincian ganti meter
		case "060912":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL);
			$rek_thn	= date('Y');
			break;
                // rincian MBR
                case "060913":
                        $data2[]        = array("kopel"=>"99_Seluruh Cabang","kp_ket"=>"[99] Seluruh Cabang");
			$data3[]	= array();
                        $parm1          = array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
                        $parm3          = array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL,"disabled"=>"disabled");
                        $rek_thn        = date('Y');
                        break;
		case "060108":
			$data3[]	= array("gol_kode"=>"KW","gol_ket"=>"Wilayah");
			$data3[]	= array("gol_kode"=>"KG","gol_ket"=>"Golongan");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>date('n'));
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL,"disabled"=>"disabled");
			break;
		default :
			$disabled	= "disabled";
			$data3[]	= array("gol_kode"=>"-","gol_ket"=>"-");
			$parm1	 	= array("class"=>"cetak","name"=>"rek_bln","selected"=>$rek_bln,"disabled"=>"disabled");
			$parm3 		= array("class"=>"cetak","name"=>"gol_kode","selected"=>NULL,"disabled"=>"disabled");
	}
	
	$data1[] = array("rek_bln"=>"1","bln_nama"=>"Januari");
	$data1[] = array("rek_bln"=>"2","bln_nama"=>"Februari");
	$data1[] = array("rek_bln"=>"3","bln_nama"=>"Maret");
	$data1[] = array("rek_bln"=>"4","bln_nama"=>"April");
	$data1[] = array("rek_bln"=>"5","bln_nama"=>"Mei");
	$data1[] = array("rek_bln"=>"6","bln_nama"=>"Juni");
	$data1[] = array("rek_bln"=>"7","bln_nama"=>"Juli");
	$data1[] = array("rek_bln"=>"8","bln_nama"=>"Agustus");
	$data1[] = array("rek_bln"=>"9","bln_nama"=>"September");
	$data1[] = array("rek_bln"=>"10","bln_nama"=>"Oktober");
	$data1[] = array("rek_bln"=>"11","bln_nama"=>"November");
	$data1[] = array("rek_bln"=>"12","bln_nama"=>"Desember");
?>
<h2 class="cetak"><?php echo _NAME; ?></h2>
<input type="hidden" id="norefresh" value="1" />
<input type="hidden" class="cetak" 	name="appl_kode" value="<?php echo _KODE; 	?>"/>
<input type="hidden" class="cetak" 	name="appl_name" value="<?php echo _NAME; 	?>"/>
<input type="hidden" class="cetak" 	name="appl_file" value="<?php echo _FILE; 	?>"/>
<input type="hidden" class="cetak" 	name="appl_proc" value="<?php echo _PROC; 	?>"/>
<input type="hidden" class="cetak" 	name="targetUrl" value="<?php echo _PROC; 	?>"/>
<input type="hidden" class="cetak" 	name="appl_tokn" value="<?php echo _TOKN; 	?>"/>
<div class="span-4 cetak">Kota Pelayanan</div>
<div class="span-7 cetak">
	: 
	<?php echo pilihan($data2,$parm2); ?>
</div>
<br/><br/>
<div class="span-4 cetak">Bulan - Tahun</div>
<div class="span-7 cetak">
	: 
	<?php echo pilihan($data1,$parm1); ?>
	<input <?=$disabled?> type="text" class="cetak" name="rek_thn" size="4" maxlength="4" value="<?php echo $rek_thn; ?>"/>
</div>
<br/><br/>
<div class="span-4 cetak">Kategori</div>
<div class="span-7 cetak">
	: 
	<?php echo pilihan($data3,$parm3); ?>
</div>
<br/><br/>
<div class="span-4 cetak">&nbsp;</div>
<div class="span-7 cetak">&nbsp;
	<input type="Button" value="Cetak" onclick="nonghol('cetak')"/>
</div>
