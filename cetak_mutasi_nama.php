<?php
	if($erno) die();
	define("_KOTA",$_SESSION['Kota_c']);
	if($proses=="cetak"){
		try{
			$wsdl_url 	= "http://"._PRIN."/printClient/printServer.wsdl";
			$client   	= new SoapClient($wsdl_url, array('cache_wsdl' => WSDL_CACHE_NONE) );
			$cetak 		= true;
		}
		catch (Exception $e){
			echo $e->getMessage();
			$cetak 		= false;
		}
		$stringFile	  = _TOKN.".txt";
		$client->cetak($stringCetak,$stringFile);
	}
	else{
		$formId 	= getToken();
		$kar_nama	= $_SESSION['Name_c'];

		// create a new cURL resource
		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, 'http://core-data.tirtaintan.co.id/report/mutasi_nama.php?tahun='.$rek_thn.'&bulan='.$rek_bln.'&kopel='.substr($kopel,0,2));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// grab URL and pass it to the browser
		if(curl_exec($ch) === false){
			$res0 = array();
		}
		else{
			$res0 = json_decode(curl_exec($ch));
			$info = curl_getinfo($ch);
			$hint = "Retrieve ".$info['size_download']."/".$info['total_time'];
		}
		
		// close cURL resource, and free up system resources
		curl_close($ch);
?>
<div id="<?php echo $formId; ?>" class="peringatan">
<input type="hidden" id="norefresh" 	value="1"/>
<input type="hidden" id="keyProses0" 	value="1"/>
<input type="hidden" id="tutup" 		value="<?php echo $formId; ?>"/>
<input type="hidden" class="cetak" name="targetUrl"	value="cetak_lpp_rekap.php"/>
<input type="hidden" class="cetak" name="targetId"	value="targetId"/>
<input type="hidden" class="cetak" name="proses" 	value="cetak"/>
<div id="targetId"></div>
<div class="pesan pull-4 span-22 prepend-top">
<div class="span-14 right large cetak">
	[<a title="Tutup jendela ini" onclick="tutup('<?php echo $formId; ?>')">Tutup</a>]
	[<a onclick="window.print()">Cetak</a>]
</div>
<table width="100%" class="prn_table">
  <tr>
	<td colspan="4" class="center"><h3><?php echo $tittle; ?></h3></td>
  </tr>
  <tr>
    <td width="20%">Pelaksana</td>
    <td width="40%">: <?php echo $kar_nama; ?></td>
    <td width="15%">&nbsp;</td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr>
    <td width="20%">Periode</td>
    <td width="40%">: <?php echo $rek_bln." - ".$rek_thn; ?></td>
    <td width="15%">&nbsp;</td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr>
    <td width="20%">Tanggal Cetak</td>
    <td width="40%">: <?php echo date('Y-m-d H:i:s'); ?></td>
    <td width="15%">&nbsp;</td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4"><?php echo $hint; ?></td>
  </tr>
</table>
<hr/>
<?php
		// line untuk ff continous paper
		$stringCetak  = chr(27).chr(67).chr(11);
		// enable paper out sensor
		$stringCetak .= chr(27).chr(57);
		// draft mode
		$stringCetak .= chr(27).chr(120).chr(48);
		// line spacing x/72
		$stringCetak .= chr(27).chr(65).chr(12);
?>
<table width="100%" class="prn_table">
	<tr class="table_cont_btm center">
		<td class="center prn_cell">No.</td>
		<td class="center prn_cell">No. SL</td>
		<td class="center prn_cell">Alamat</td>
		<td class="center prn_cell">Nama</td>
		<td class="center prn_cell">Remark</td>
		<td class="center prn_cell">Status</td>
	        <td class="center prn_cell">Pelaksana</td>
	        <td class="center prn_cell">Tanggal</td>
	</tr>
<?php
		if(count($res0)>0){
			$nomer		= 0;
			$level1_val 	= (array) $res0;
			$level1_key 	= array_keys($level1_val);
			/* order by level 1 */
			for($i=0;$i<count($level1_val);$i++){
				$level2_val	= (array) $level1_val[$level1_key[$i]];
				$level2_key	= array_keys($level2_val);

				/* order by level 2 */
				for($k=0;$k<count($level2_val);$k++){
					$nomer++;
					$warna	= "black";
					$nilai	= (array) $level2_val[$level2_key[$k]];
					$klas 	= "table_cell1";
					if(($k%2) == 0){
						$klas = "table_cell2";
					}
					$kunci	= array_keys($nilai);
					for($m=0;$m<count($kunci);$m++){
						$$kunci[$m] = $nilai[$kunci[$m]];
					}

					//$l0_lembar[] 	= $lembar;
					//$l1_lembar[$level1_key[$i]][]	= $lembar;
					if(isset($pel_nosl)){
						if($pel_nosl == $pel_no){
							$warna	= "red";
							$nomer--;
						}
					}
					$pel_nosl	= $pel_no;
					
					
					if($k==0){
?>
	<tr><th colspan="13" class="prn_left"><?php echo $kp_ket; ?></th></tr>
<?php
					}
?>
	<tr class="<?php echo $klas; ?>">
		<td class="right prn_cell prn_left" style="color: <?=$warna?>"><?php echo ($k+1); ?></td>
		<td class="right prn_cell" style="color: <?=$warna?>"><?php echo $pel_no;			?></td>
		<td class="left prn_cell" style="color: <?=$warna?>"><?php echo $pel_alamat; 			?></td>
		<td class="left prn_cell" style="color: <?=$warna?>"><?php echo $pel_nama;			?></td>
		<td class="left prn_cell" style="color: <?=$warna?>"><?php echo $remark; 			?></td>
		<td class="left prn_cell" style="color: <?=$warna?>"><?php echo $kps_ket; 			?></td>
	        <td class="right prn_cell" style="color: <?=$warna?>"><?php echo $kar_id;			?></td>
	        <td class="right prn_cell" style="color: <?=$warna?>"><?php echo substr($remark_tgl,0,10);	?></td>
	</tr>
<?php
				}
			}
			$level1_val 	= $l1_pakai;
			$level1_key 	= array_keys($level1_val);
			/* order by level 1 pelanggan */
			for($i=0;$i<count($level1_val);$i++){
				//$c1_lembar		= array_sum($l1_lembar[$level1_key[$i]]);
?>
	<tr class="table_cont_btm">
	</tr>
<?php
			}
			//$c0_lembar	= array_sum($l0_lembar);
			if($nomer>0){
?>
    <tr class="table_cont_btm">
    	<td class="right prn_cell"><?php echo $nomer; ?></td>
	<td colspan="7" class="right prn_cell"></td>
    </tr>
<?php
			}
		}
?>
</table>
<?php
		//$stringFile	  = "_data/"._TOKN.".txt";
		//$openFile 	  = fopen($stringFile, 'w');
		//fwrite($openFile, $stringCetak);
		//fclose($openFile);
?>
<input type="hidden" class="cetak" name="stringCetak" value="<?php echo base64_encode($stringCetak); ?>"/>
</div>
</div>
<?php
	}

