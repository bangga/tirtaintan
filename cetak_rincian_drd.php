<?php
	if($erno) die();
	$formId 	= getToken();
	$kp_kode	= explode("_",$kopel);
	if($gol_kode=='KG'){
		$que0 = "SELECT CONCAT(a.pel_no,' [',IFNULL(c.ref_no,CONCAT(SUBSTR(a.dkd_kd,1,2),'.',SUBSTR(a.dkd_kd,3,2),'.0',SUBSTR(a.dkd_kd,5,2),'.000')),']') AS pel_no,a.pel_nama,a.rek_gol AS gol_kode,a.rek_gol AS dkd_kd,a.rek_stanlalu,a.rek_stankini,(a.rek_stankini-a.rek_stanlalu) AS rek_pakai,a.rek_uangair,(a.rek_meter+a.rek_adm) AS rek_beban,a.rek_angsuran,a.rek_total,CONCAT('GOLONGAN : [',b.gol_kode,'] ',b.gol_ket) AS rayon FROM tm_drd_awal a JOIN tr_gol b ON(b.gol_kode=a.rek_gol) JOIN tm_pelanggan d ON(d.pel_no=a.pel_no) LEFT JOIN ref_pelanggan c ON(c.pel_no=a.pel_no) WHERE a.rek_bln=$rek_bln AND a.rek_thn=$rek_thn AND d.kp_kode='".$kp_kode[0]."' ORDER BY a.rek_gol,a.pel_no";
	}
	else{
		$que0 = "SELECT CONCAT(a.pel_no,' [',IFNULL(c.ref_no,CONCAT(SUBSTR(a.dkd_kd,1,2),'.',SUBSTR(a.dkd_kd,3,2),'.0',SUBSTR(a.dkd_kd,5,2),'.000')),']') AS pel_no,a.pel_nama,a.rek_gol AS gol_kode,a.dkd_kd,a.rek_stanlalu,a.rek_stankini,(a.rek_stankini-a.rek_stanlalu) AS rek_pakai,a.rek_uangair,(a.rek_meter+a.rek_adm) AS rek_beban,a.rek_angsuran,a.rek_total,CONCAT('JALAN : [',b.dkd_kd,'] ',b.dkd_jalan) AS rayon FROM tm_drd_awal a JOIN tr_dkd b ON(b.dkd_kd=a.dkd_kd) JOIN tm_pelanggan d ON(d.pel_no=a.pel_no) LEFT JOIN ref_pelanggan c ON(c.pel_no=a.pel_no) WHERE a.rek_bln=$rek_bln AND a.rek_thn=$rek_thn AND d.kp_kode='".$kp_kode[0]."' ORDER BY a.dkd_kd,a.pel_no";
	}
	try{
		$res0 = mysql_query($que0,$link);
		while($row0 = mysql_fetch_array($res0)){
			$data[$row0['dkd_kd']][] = $row0;
		}
	}
	catch (Exception $e){
		errorLog::errorDB(array($que0));
		$mess = $e->getMessage();
	}
	// line untuk ff continous paper
	$stringCetak  = chr(27).chr(67).chr(1);
	// enable paper out sensor
	$stringCetak .= chr(27).chr(57);
	// mode 12 cpi
	$stringCetak .= chr(27).chr(77);
	// draft mode
	$stringCetak .= chr(27).chr(120).chr(48);
	// line spacing x/72
	$stringCetak .= chr(27).chr(65).chr(12);

	$baris 		= 3;
	$halaman	= 1;
	$totalBaris	= 56;
	if(count($data)>0){
		$level1_val 	= $data;
		$level1_key 	= array_keys($level1_val);
		/* order by level 1 pelanggan */
		for($i=0;$i<count($level1_val);$i++){
			// resume bisa ditambahkan di sini
			$rayon	= $level1_val[$level1_key[$i]][0][11];
			if($halaman==1){
				$stringCetak .= printCenter("PERUSAHAAN DAERAH AIR MINUM TIRTA INTAN",140).printLeft("TGL. ".date('d/m/Y'),20).chr(10);
				$baris++;
				if(($baris%$totalBaris)==0){
					$halaman++;
					$stringCetak .= printCenter(" ",140).printLeft("Halaman ".$halaman,20).chr(10);
					$baris++;
				}
				$stringCetak .= printCenter("DAFTAR REKENING YANG AKAN DITAGIH (DRD-AIR) UNTUK BULAN : ".$bulan[$rek_bln]." ".$rek_thn,140).printLeft("HALAMAN ".$halaman,20).chr(10);
				$baris++;
				if(($baris%$totalBaris)==0){
					$halaman++;
					$stringCetak .= printCenter(" ",140).printLeft("Halaman ".$halaman,20).chr(10);
					$baris++;
				}
			}
			$stringCetak .= printLeft($kp_kode[1],60).printLeft($rayon,100).chr(10);
			$baris++;
			if(($baris%$totalBaris)==0){
				$halaman++;
				$stringCetak .= printCenter(" ",140).printLeft("Halaman ".$halaman,20).chr(10);
				$baris++;
			}
			$stringCetak .= str_repeat('=',160).chr(10);
			$baris++;
			if(($baris%$totalBaris)==0){
				$halaman++;
				$stringCetak .= printCenter(" ",140).printLeft("Halaman ".$halaman,20).chr(10);
				$baris++;
			}
			$stringCetak .= printRight("NO.",5);
			$stringCetak .= printLeft(" NOMOR SL",25);
			$stringCetak .= printLeft("NAMA",45);
			$stringCetak .= printRight("GOL.",5);
			$stringCetak .= printRight("LALU",10);
			$stringCetak .= printRight("KINI",10);
			$stringCetak .= printRight("PAKAI",10);
			$stringCetak .= printRight("HARGA AIR",15);
			$stringCetak .= printRight("BEA BEBAN",10);
			$stringCetak .= printRight("ANGSURAN",10);
			$stringCetak .= printRight("TOTAL",15).chr(10);
			$baris++;
			if(($baris%$totalBaris)==0){
				$halaman++;
				$stringCetak .= printCenter(" ",140).printLeft("Halaman ".$halaman,20).chr(10);
				$baris++;
			}
			$stringCetak .= str_repeat('=',160).chr(10);
			$baris++;
			if(($baris%$totalBaris)==0){
				$halaman++;
				$stringCetak .= printCenter(" ",140).printLeft("Halaman ".$halaman,20).chr(10);
				$baris++;
			}
				
			$level2_val		= $level1_val[$level1_key[$i]];
			$level2_key		= array_keys($level2_val);
			/* order by level 2 rincian tunggakan */
			for($k=0;$k<count($level2_val);$k++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $level2_val[$level2_key[$k]];
				$konci	= array_keys($nilai);
				for($l=0;$l<count($konci);$l++){
					$$konci[$l]	= $nilai[$konci[$l]];
				}
				/* getParam **/
				$stringCetak .= printRight(($k+1),5);
				$stringCetak .= printLeft(" ".$pel_no,25);
				$stringCetak .= printLeft($pel_nama,45);
				$stringCetak .= printRight($gol_kode,5);
				$stringCetak .= printRight(number_format($rek_stanlalu),10);
				$stringCetak .= printRight(number_format($rek_stankini),10);
				$stringCetak .= printRight(number_format($rek_pakai),10);
				$stringCetak .= printRight(number_format($rek_uangair),15);
				$stringCetak .= printRight(number_format($rek_beban),10);
				$stringCetak .= printRight(number_format($rek_angsuran),10);
				$stringCetak .= printRight(number_format($rek_total),15).chr(10);
				$baris++;
				if(($baris%$totalBaris)==0){
					$halaman++;
					$stringCetak .= printCenter(" ",140).printLeft("Halaman ".$halaman,20).chr(10);
					$baris++;
				}
				$l0_pakai[$i][]		= $rek_pakai;
				$l0_uangair[$i][]	= $rek_uangair;
				$l0_beban[$i][]		= $rek_beban;
				$l0_angsuran[$i][]	= $rek_angsuran;
				$l0_total[$i][]		= $rek_total;
			}
			$stringCetak .= str_repeat('=',160).chr(10);
			$baris++;
			if(($baris%$totalBaris)==0){
				$halaman++;
				$stringCetak .= printCenter(" ",140).printLeft("Halaman ".$halaman,20).chr(10);
				$baris++;
			}
			$stringCetak .= printRight("Jumlah Akhir :",100);
			$stringCetak .= printRight(number_format(array_sum($l0_pakai[$i])),10);
			$stringCetak .= printRight(number_format(array_sum($l0_uangair[$i])),15);
			$stringCetak .= printRight(number_format(array_sum($l0_beban[$i])),10);
			$stringCetak .= printRight(number_format(array_sum($l0_angsuran[$i])),10);
			$stringCetak .= printRight(number_format(array_sum($l0_total[$i])),15).chr(10);
			$baris++;
			if(($baris%$totalBaris)==0){
				$halaman++;
				$stringCetak .= printCenter(" ",140).printLeft("Halaman ".$halaman,20).chr(10);
				$baris++;
			}
			$stringCetak .= str_repeat('=',160).chr(10);
			$baris++;
			if(($baris%$totalBaris)==0){
				$halaman++;
				$stringCetak .= printCenter(" ",140).printLeft("Halaman ".$halaman,20).chr(10);
				$baris++;
			}
			$stringCetak .= chr(10);
			$baris++;
			if(($baris%$totalBaris)==0){
				$halaman++;
				$stringCetak .= printCenter(" ",140).printLeft("Halaman ".$halaman,20).chr(10);
				$baris++;
			}
		}
	}
	$stringCetak .= chr(12);
	//$stringFile	  = "_data/"._TOKN.".txt";
	//$openFile 	  = fopen($stringFile, 'w');
	//fwrite($openFile, $stringCetak);
	//fclose($openFile);
		
	try{
		$wsdl_url 	= "http://"._PRIN."/printClient/printServer.wsdl";
		$client   	= new SoapClient($wsdl_url, array('cache_wsdl' => WSDL_CACHE_NONE) );
		$cetak 		= true;
	}
	catch (Exception $e){
		echo $e->getMessage();
		$cetak 		= false;
	}
	$stringFile	  = _TOKN.".txt";
	$client->cetak(base64_encode($stringCetak),$stringFile);
?>
<div id="<?php echo $formId; ?>" class="peringatan">
<input type="hidden" id="keyProses0" value="2"/>
<input type="hidden" id="tutup" value="<?php echo $formId; ?>"/>
<div class="pesan form-5 prepend-top">
<div class="span-14 right large cetak">
	[<a title="Tutup jendela ini" onclick="tutup('<?php echo $formId; ?>')">Tutup</a>]
	<br/><div class="info">Cetak DRD Sedang dilakukan. Klik <b>Tutup</b> untuk menutup jendela ini.</div>
</div>
</div>
</div>