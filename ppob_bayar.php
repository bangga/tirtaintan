<?php
	if($erno) die();
	$kar_id = _USER;
	$byr_no	= _TOKN;
	$develp = false;
	
	/** koneksi ke database */
	$db		= false;
	try {
		$db 	= new PDO($PSPDO[0],$PSPDO[1],$PSPDO[2]);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		unset($mess);
	}
	catch (PDOException $err){
		$mess = $err->getTrace();
		errorLog::errorDB(array($mess[0]['args'][0]));
		$mess = "Mungkin telah terjadi kesalahan pada database server, sehingga koneksi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini.";
		$klas = "error";
	}
	switch($proses){
		case "setResi":
			$db->beginTransaction();
			if($db){
				try {
					$que	= "INSERT INTO system_parameter(sys_param,sys_value,sys_value1,sys_value2) VALUES('RESI','$kar_id','$noresi','-') ON DUPLICATE KEY UPDATE sys_value1='$noresi'";
					$st 	= $db->exec($que);
					if($st>0){
						$db->commit();
						errorLog::logDB(array($que));
						$mess = "Set resi: $noresi telah dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini.";
					}
					else{
						$db->rollBack();
						$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses set resi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini.";
					}
				}
				catch (PDOException $err){
					$db->rollBack();
					$mess = $err->getTrace();
					errorLog::errorDB(array($mess[0]['args'][0]));
					$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses set resi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini.";
				}
			}
			break;
		case "bayar":
			$cetak	= true;
			if($cetak){
				$db->beginTransaction();
				$j 		= 0;
				$erno	= false;
				if($kembalian>=0){				
					if(!is_array($pel_no)){
						$tmp_no 	= $pel_no;
						$tmp_nama 	= $pel_nama;
						$tmp_alamat	= $pel_alamat;
						unset($pel_no);
						unset($pel_nama);
						unset($pel_alamat);
						for($i=0;$i<=count($pilih);$i++){
							$pel_no[$i] 	= $tmp_no;
							$pel_nama[$i] 	= $tmp_nama;
							$pel_alamat[$i]	= $tmp_alamat;
						}
					}
					for($i=0;$i<=count($pilih);$i++){
						if($db && $pilih[$i]==1){
							try {
								// line untuk ff continous paper
								$stringCetak  = chr(27).chr(67).chr(3);
								// enable paper out sensor
								$stringCetak .= chr(27).chr(57);
								// draft mode
								$stringCetak .= chr(27).chr(120).chr(48);
								// line spacing x/72
								$stringCetak .= chr(27).chr(65).chr(12);
								$stringCetak .= chr(10).chr(10).chr(10).chr(10);
								$stringCetak .= "REK.BULAN   : ".printRight($bulan[$rek_bln[$i]]." ".$rek_thn[$i],16).	"  REK.BULAN   : ".$bulan[$rek_bln[$i]]." ".$rek_thn[$i].chr(10);
								$stringCetak .= "NO.LANGG    : ".printRight($pel_no[$i],16).							"  NO.LANGG    : ".printLeft($pel_no[$i],10).						" TARIF       : ".printRight($rek_gol[$i],9).chr(10);
								$stringCetak .= "TARIF       : ".printRight($rek_gol[$i],16).							"  NAMA        : ".$pel_nama[$i].chr(10);
								$stringCetak .= "NAMA        : ".printRight(substr($pel_nama[$i],0,16),16).				"  ALAMAT      : ".$pel_alamat[$i].chr(10);
								$stringCetak .= "ALAMAT      : ".printRight(substr($pel_alamat[$i],0,16),16).			"  HARGA AIR   : ".printRight(number_format($rek_uangair[$i]),10).	" STAND AKHIR : ".printRight(number_format($rek_stankini[$i]),9).chr(10);
								$stringCetak .= "STAND AKHIR : ".printRight(number_format($rek_stankini[$i]),16).		"  BIAYA BEBAN : ".printRight(number_format($rek_beban[$i]),10).	" STAND AWAL  : ".printRight(number_format($rek_stanlalu[$i]),9).chr(10);
								$stringCetak .= "STAND AWAL  : ".printRight(number_format($rek_stanlalu[$i]),16).		"  CICILAN BP  : ".printRight(number_format($rek_angsuran[$i]),10).	" JML.PAKAI   : ".printRight(number_format($rek_pakai[$i]),9).chr(10);
								$stringCetak .= "JML.PAKAI   : ".printRight(number_format($rek_pakai[$i]),16).			"  DENDA       : ".printRight(number_format($rek_denda[$i]),10).chr(10);
								$stringCetak .= "HARGA AIR   : ".printRight(number_format($rek_uangair[$i]),16).		"  JML.TAGIHAN : ".printRight(number_format($rek_bayar[$i]),10).chr(10);
								$stringCetak .= "BIAYA BEBAN : ".printRight(number_format($rek_beban[$i]),16).			"  TERBILANG   : ".strtoupper(substr((n2c($rek_bayar[$i],"Rupiah")),0,33)).chr(10);
								$stringCetak .= "CICILAN BP  : ".printRight(number_format($rek_angsuran[$i]),16).		"                ".strtoupper(substr((n2c($rek_bayar[$i],"Rupiah")),33,33)).chr(10);
								$stringCetak .= "DENDA       : ".printRight(number_format($rek_denda[$i]),16).			"                ".strtoupper(substr((n2c($rek_bayar[$i],"Rupiah")),66,33)).chr(10);
								$stringCetak .= "JML.TAGIHAN : ".printRight(number_format($rek_bayar[$i]),16).			"                ".strtoupper(substr((n2c($rek_bayar[$i],"Rupiah")),99,33)).chr(10);
								$stringCetak .= "REFERENSI   : ".$byr_no.												"  REFERENSI   : ".$loket.$byr_no.chr(10);
								$stringCetak .= "KASIR       : ".printRight($noresi."-".$kar_id,16).					"  KASIR       : ".$noresi."-".$kar_id.chr(10);
								if($rek_materai[$i]>0){
									$stringCetak .= printRight("Mat. ".$rek_materai[$i],77);
								}
								$stringCetak .= chr(12);
								
								$que	= "UPDATE tm_rekening SET rek_denda=$rek_denda[$i],rek_byr_sts=1 WHERE rek_nomor=$rek_nomor[$i] AND rek_sts=1 AND rek_byr_sts=0 AND (rek_total+$rek_denda[$i])=$rek_bayar[$i]";
								$st 	= $db->exec($que);
								if($st>0){
									errorLog::logDB(array($que));
								}
								else{
									$erno 	= true;
									$mess	= "Gagal update tabel rekening.";
									$i		= count($pilih) + 1;
								}
								$que	= "INSERT INTO tm_pembayaran(byr_no,byr_tgl,byr_serial,rek_nomor,kar_id,lok_ip,byr_loket,byr_total,byr_cetak,byr_upd_sts,byr_sts) VALUES(".$byr_no.",'".$byr_tgl." 00:00:00','".$noresi."',".$rek_nomor[$i].",'".$kar_id."','".$ipClient."','".$loket."',".$rek_bayar[$i].",0,NOW(),1)";
								$st 	= $db->exec($que);
								if($st>0){
									errorLog::logDB(array($que));
									$noresi++;
								}
								else{
									$erno 	= true;
									$mess	= "Gagal insert tabel pembayaran.";
									$i		= count($pilih) + 1;
								}
								$j++;
							}
							catch (PDOException $err){
								$erno 	= true;
								$i		= count($pilih) + 1;
								$mess 	= "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses pembayaran tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini.";
								$klas	= "error";
								errorLog::errorDB(array($que));
							}
						}
					}
					if($erno){
						$db->rollBack();
						$j=0;
					}
					else{
						if($develp){
							$db->rollBack();
						}
						else{
							$db->commit();
						}						
					}
					if($j>0){
						$mess	= "$j transaksi telah dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula.";
						$klas 	= "success";
					}
					else{
						$mess	= "$j transaksi telah dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula.";
						$klas 	= "error";
					}
				}
				else{
					$mess	= "Uang yang diterima tidak mencukupi. Tekan tombol <b>B</b> untuk kembali ke halaman semula.";
					$klas 	= "error";
				}
			}
			break;
		default:
			$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses pembayaran tidak bisa dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula.";
			$klas = "error";
	}
	errorLog::logMess(array($mess));
	echo "<input type=\"hidden\" id=\"$errorId\" value=\"$mess\"/>";
	unset($db);
?>