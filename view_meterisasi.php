<?php
	if($erno) die();
	$formId = getToken();
	$formDump = 0;

	/* koneksi database */
	/* link : link baca */
	$link 	= mysql_connect($DHOST,$DUSER,$DPASS) or die(errorLog::errorDie(array(mysql_error())));
	mysql_select_db($DNAME,$link) or die(errorLog::errorDie(array(mysql_error())));		

	// filter akses untuk admin
	if($_SESSION['Group_c']=='000'){
		$readonly = "";
		$disabled = "";
		$filtered = "WHERE kp_kode='".$kp_kode."'";
	}
	else{
		$readonly = "readonly";
		$disabled = "disabled";
		$filtered = "WHERE dkd_kd='".$dkd_kd."'";
	}

	switch($proses){
		default:
			/* retrieve data survey */
                        $checked      = "";
                        $meterisasi   = 0;
                        $stand_angkat = 0;
                        $stand_pasang = 0;
                        $readonly     = "readonly";
                        $disabled     = "disabled";
			if($kps_kode==17 OR $kps_kode==21){
				// create a new cURL resource
				$ch 	= curl_init();

				// set URL and other appropriate options
				curl_setopt($ch, CURLOPT_URL, 'http://core-data.tirtaintan.co.id/order/pull/'.$pel_no);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				$survey	= json_decode(curl_exec($ch));

				// close cURL resource, and free up system resources
				curl_close($ch);

				foreach ($survey->data->$pel_no as $key => $value){
					// retrieve hasil ukuran meter
					if($value->order_kode==2 && $value->result_kode==6 && ($kps_kode==4 || $kps_kode==17 || $kps_kode==21)){
						$um_kode = $value->result_value;
					}
					// retrieve stand pasang
					if($value->order_kode==3 && $value->result_kode==8 && ($kps_kode==4 || $kps_kode==17 || $kps_kode==21)){
						$meterisasi 	= 1;
						$stand_pasang	= $value->result_value;
						$checked	= "checked";
						$readonly 	= "";
						$disabled 	= "";
					}
				}
			}

			// buka akses grup admin untuk meterisasi
			if($_SESSION['Group_c']=='000'){
				$readonly = "";
				$disabled = "";
			}
			
			/* panduan pintasan aplikasi */
			$panduan	= true;
			if(isset($_SESSION['panduan'])){
				$panduan = true;
			}	
			$hint = "<div class=\"notice\">Tekan tombol <b>Enter</b> untuk untuk memulai entry data, <b>Alt+M</b> untuk meterisasi , kemudian <b>Tab</b> untuk mengisi stand pasang, kemudian <b>Alt+S</b> untuk menyimpan, dan tombol <b>Esc</b> untuk menutup halaman ini.</div>";
?>
<div id="<?php echo $formId; ?>" class="peringatan">
<input id="keyProses0" 	type="hidden" value="1"/>
<input id="tutup" 	type="hidden" value="<?php echo $formId; ?>" />
<div class="pesan form-5">
<div class="span-20 right">[<a title="Tutup jendela ini" onclick="tutup('<?php echo $formId; ?>')">Tutup</a>]</div>
<br/><h3>Form Data Pelanggan</h3>
<hr/>

<input type="hidden" class="pilih" 	name="targetUrl" 	value="<?php echo __FILE__;	?>"/>
<input type="hidden" class="pilih" 	name="proses" 		value="pilihRayon"/>
<input type="hidden" class="pilih"	name="targetId" 	value="targetRayon"/>
<input type="hidden" class="pilih"	name="dump" 		value="0"/>
<input type="hidden" class="simpan"	name="appl_tokn" 	value="<?php echo _TOKN; 	?>"/>
<input type="hidden" class="simpan"	name="appl_kode" 	value="<?php echo _KODE; 	?>"/>
<input type="hidden" class="simpan"	name="targetUrl" 	value="<?php echo _PROC; 	?>"/>
<input type="hidden" class="simpan"	name="targetId" 	value="targetUpdate"/>
<input type="hidden" class="simpan"	name="proses" 		value="meterisasi"/>
<input type="hidden" class="simpan"	name="dump" 		value="<?php echo $formDump;	?>"/>
<input type="hidden" class="simpan" 	name="pel_no" 		value="<?php echo $pel_no; 	?>"/>
<input type="hidden" class="simpan"     name="stand_angkat"     value="<?php echo $stand_angkat;?>"/>
<input type="hidden" class="simpan"     name="meterisasi"       value="1" />
<div>
	<div class="span-9 left border">
		<div class="append-bottom span-3">No Pelanggan</div>
		<div class="append-bottom span-5">: <?php echo $pel_no;			?></div>
		<div class="append-bottom span-3">Kota Pelayanan</div>
		<div class="append-bottom span-5">: <?php echo $kp_ket;			?></div>
		<div class="append-bottom span-3">Nama</div>
		<div class="append-bottom span-5">: <?php echo $pel_nama;		?></div>
		<div class="append-bottom span-3">Alamat</div>
		<div class="append-bottom span-5">: <?php echo $pel_alamat;		?></div>
		<div class="append-bottom span-3">Golongan</div>
		<div class="append-bottom span-5">: <?php echo $gol_kode;		?></div>
		<div class="append-bottom span-3">Rayon</div>
		<div class="append-bottom span-5">: <?php echo $dkd_kd;			?></div>
		<div class="append-bottom span-3">Ukuran Meter</div>
		<div class="append-bottom span-5">: <?php echo $um_ket;			?></div>
		<div class="append-bottom span-3">Meterisasi</div>
		<div class="append-bottom span-5">: <?php echo $met_tgl;		?></div>
		<div class="append-bottom span-3">Status</div>
		<div class="append-bottom span-5">: <?php echo $kps_ket;		?></div>
	</div>
	<div class="span-13 left">
		<div class="append-bottom span-3">Meterisasi</div>
		<div class="append-bottom span-7">
			<input <?php echo $readonly; ?> id="form-1" type="text" class="simpan" name="stand_pasang" maxlength="9" value="<?php echo $stand_pasang; ?>" />
		</div>
		<div id="targetUpdate" class="span-12">&nbsp;
			<input id="form-2" accesskey="S" type="button" value="Simpan" onclick="buka('simpan')"/>
			<input id="jumlahForm" type="hidden" value="2" />
			<input id="aktiveForm" type="hidden" value="0" />
		</div>
	</div>
</div>
</div>
</div>
<?php
	}

