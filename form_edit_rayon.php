<?php
	if($erno) die();
	$formId 	= getToken();
	$targetId 	= getToken();
	$errorId 	= getToken();
	
	/* inquiry kota pelayanan */
	$kopel	= $_SESSION['Kota_c']."_".$_SESSION['kp_ket'];
	if($_SESSION['Group_c']=='000'){
		$filtered = '';
	}
	else if($_SESSION['c_group']=='00'){
		$filtered = '';
	}
	else{
		$filtered = "WHERE kp_kode='".$_SESSION['Kota_c']."'";
	}
	
	try{
		$que3 = "SELECT CONCAT(kp_kode,'_',kp_ket) AS kopel,CONCAT('[',kp_kode,'] ',kp_ket) AS kp_ket FROM tr_kota_pelayanan $filtered ORDER BY kp_kode ASC";
		if(!$res3 = mysql_query($que3,$link)){
			throw new Exception("Terjadi kesalahan pada sistem database<br/>Nomor Tiket : ".substr(_TOKN,-4));
		}
		else{
			while($row3 = mysql_fetch_array($res3)){
				$data3[] = array("kopel"=>$row3['kopel'],"kp_ket"=>$row3['kp_ket']);
			}
			$mess = false;
		}
	}
	catch (Exception $e){
		errorLog::errorDB(array($que3));
		$mess = $e->getMessage();
		$erno = false;
	}
	$parm3 		= array("class"=>"simpan","id"=>"rayn-1","name"=>"kopel","selected"=>$kopel);
	
	/* inquiry karyawan */
	try{
		$que2 = "SELECT kar_id,kar_nama FROM tm_karyawan WHERE grup_id='002' OR kar_id='admin' ORDER BY kar_nama";
		if(!$res2 = mysql_query($que2,$link)){
			throw new Exception("Terjadi kesalahan pada sistem database<br/>Nomor Tiket : ".substr(_TOKN,-4));
		}
		else{
			while($row2 = mysql_fetch_array($res2)){
				$data2[] = array("kar_id"=>$row2['kar_id'],"kar_nama"=>$row2['kar_nama']);
			}
			$mess = false;
		}
	}
	catch (Exception $e){
		errorLog::errorDB(array($que2));
		$mess = $e->getMessage();
		$erno = false;
	}
	$parm2 = array("class"=>"simpan","id"=>"rayn-5","name"=>"kar_id","selected"=>$kar_id);
?>
<div id="<?php echo $formId; ?>" class="peringatan">
	<input id="keyProses0" 	type="hidden" value="1"/>
	<input id="tutup" 		type="hidden" value="<?php echo $formId; ?>" />
	<div class="pesan span-18">
		<div class="span-18 right large">[<a title="Tutup jendela ini" onclick="tutup('<?php echo $formId; ?>')">Tutup</a>]</div>
		<h3>Form Edit Data Rayon</h3>
		<hr/>
		<div id="<?php echo $targetId; ?>">
<?php
			if(_HINT==1){
?>
			<div class="notice left">Tekan tombol <b>Enter</b> untuk memulai proses entryan, <b>Alt+S</b> untuk menyimpan dan <b>Delete</b> untuk menghapus.</div>
<?php
			}
?>
		</div>
		<div class="span-9 left">
			<div class="span-2 prepend-top">Unit Layanan</div>
			<div class="span-5 prepend-top">: <?=pilihan($data3,$parm3)?></div>
			<div class="span-2 prepend-top">Rayon</div>
			<div class="span-5 prepend-top">:
				<input id="rayn-2" type="text" maxlength="2" size="20" class="simpan" name="dkd_rayon" value="<?php echo $dkd_rayon; ?>" onmouseover="$(this.id).select()" />
			</div>
			<div class="span-2 prepend-top">Nomor</div>
			<div class="span-5 prepend-top">:
				<input id="rayn-3" type="text" maxlength="2" size="20" class="simpan" name="dkd_no" value="<?php echo $dkd_no; ?>" onmouseover="$(this.id).select()" />
			</div>
			<div class="span-2 prepend-top">Kode DKD</div>
			<div class="span-5 prepend-top">: <?php echo $dkd_kd; ?></div>
			<div class="span-2 prepend-top">Jalan/Lokasi</div>
			<div class="span-5 prepend-top">
				<textarea id="rayn-4" class="simpan height-2" name="dkd_jalan" onmouseover="$(this.id).select()"><?php echo $dkd_jalan; ?></textarea>
			</div>
			<div class="span-2 prepend-top">Pembaca</div>
			<div class="span-5 prepend-top">: <?=pilihan($data2,$parm2)?></div>
			<div class="span-2 prepend-top">Tgl. Catat</div>
			<div class="span-5 prepend-top">:
				<input id="rayn-6" type="text" maxlength="2" size="20" class="simpan" name="dkd_tcatat" value="<?php echo $dkd_tcatat; ?>" onmouseover="$(this.id).select()" />
			</div>
			<div class="span-2 prepend-top">&nbsp;</div>
			<div class="span-5 prepend-top">&nbsp;
				<input type="hidden" class="simpan delete" 	name="targetId" 	value="<?php echo $targetId;?>"/>
				<input type="hidden" class="simpan delete" 	name="errorId" 		value="<?php echo $errorId;	?>"/>
				<input type="hidden" class="simpan delete" 	name="dkd_kd" 		value="<?php echo $dkd_kd;	?>"/>
				<input type="hidden" class="simpan delete" 	name="targetUrl"	value="<?php echo _PROC; 	?>"/>
				<input type="hidden" class="simpan delete" 	name="dump"			value="0"/>
				<input type="hidden" class="simpan" 		name="proses"		value="editRayon"/>
				<input type="hidden" class="delete" 		name="proses"		value="deleteRayon"/>
				<input type="button" value="Simpan" onclick="buka('simpan')" accesskey="S" />
				<input type="button" value="Delete" onclick="buka('delete')"/>
				<input id="jumlahRayn" type="hidden" value="6"/>
				<input id="aktiveRayn" type="hidden" value="0"/>
			</div>
		</div>
	</div>
</div>