<?php
	if($erno) die();
	$formId 	= getToken();
	if(!isset($tgl_awal)){
		$tgl_awal	= 1;
	}
	if(!isset($tgl_akhir)){
		$tgl_akhir	= 1;
	}
	/* koneksi database */
	/* link : link baca */
	$mess 	= "user : ".$DUSER." tidak bisa terhubung ke server : ".$DHOST;
	$link 	= mysql_connect($DHOST,$DUSER,$DPASS) or die(errorLog::errorDie(array($mess)));
	try{
		if(!mysql_select_db($DNAME,$link)){
			throw new Exception("user : ".$DUSER." tidak bisa terhubung ke database : ".$DNAME);
		}
	}
	catch (Exception $e){
		errorLog::errorDB(array($e->getMessage()));
		$mess = "Terjadi kesalahan pada sistem<br/>Nomor Tiket : ".substr(_TOKN,-4);
		$klas = "error";
		$erno = false;
	}
	
	/* inquiry data drd */
	$kopel		= explode("_",$kopel);
	$que0 		= "SELECT * FROM v_ganti_meter WHERE tahun=".$rek_thn." AND bulan=".$rek_bln." AND kp_kode='".$kopel[0]."' ORDER BY kps_kode DESC,dkd_kd,pel_no"; 
	try{
		if(!$res0 = mysql_query($que0,$link)){
			throw new Exception($que0);
		}
		else{
			$i = 0;
			while($row0 = mysql_fetch_array($res0)){
				$data[$row0['pel_no']] = $row0;
				$i++;	
			}
		$mess = false;
		}
	}
	catch (Exception $e){
		$mess = $e->getMessage();
		errorLog::errorDB(array($mess));
		errorLog::logDB(array($que0));
	}
	if(strlen($gol_kode)<1){
		$gol_kode = "Seluruh Golongan";
	}
	$tgl_awal	= $tgl_awal."/".$rek_bln."/".$rek_thn;
	$tgl_akhir	= $tgl_akhir."/".$rek_bln."/".$rek_thn;
?>
<div id="<?php echo $formId; ?>" class="peringatan">
<div class="pesan form-5">
<div class="span-14 right large cetak">
	[<a title="Tutup jendela ini" onclick="tutup('<?php echo $formId; ?>')">Tutup</a>]
	[<a onclick="window.print()">Cetak</a>]
</div>
<h3><?php echo $appl_owner; ?> - <?php echo $kopel[1]; ?></h3>
<hr/>
<h4><?php echo _NAME; ?> Rekening</h4>
<table width="100%" class="prn_table">
	<tr>
		<td colspan="2">Tanggal Cetak</td>
		<td colspan="7">: <?php echo $tanggal; ?></td>
	</tr>
	<tr>
		<td colspan="2">Periode</td>
		<td colspan="7">: <?php echo $rek_bln." - ".$rek_thn; ?></td>
	</tr>
	<tr>
		<td colspan="2">Golongan</td>
		<td colspan="7">: <?php echo $gol_kode; ?></td>
	</tr>
	<tr>
		<td colspan="2">Petugas</td>
		<td colspan="6">: <?php echo _NAMA; ?></td>
	</tr>
	<tr class="table_cont_btm">
		<td rowspan="2" class="center prn_head">No.</td>
		<td rowspan="2" class="center prn_head">No. SL</td>
		<td rowspan="2" class="center prn_head">Nama</td>
		<td rowspan="2" class="center prn_head">Alamat</td>
		<td colspan="3" class="center prn_head">Meter Lama</td>
		<td colspan="3" class="center prn_head">Meter Baru</td>
		<td rowspan="2" class="center prn_head">Tanggal</td>
		<td rowspan="2" class="center prn_head">Status</td>
    </tr>
	<tr class="table_cont_btm">
		<td class="center prn_cell">Merk</td>
		<td class="center prn_cell">No. Seri</td>
		<td class="center prn_cell">Angka</td>
		<td class="center prn_cell">Merk</td>
		<td class="center prn_cell">No. Seri</td>
		<td class="center prn_cell">Angka</td>
	</tr>
<?php
	if(!isset($data)){
		$data	  = array();
	}
        $data_key = array_keys($data);

	for($i=0;$i<count($data);$i++){
		$nomor		= $i+1;
		$row0 	  	= $data[$data_key[$i]];
		$klas 	  	= "table_cell1";
		if(($i%2) == 0){
			$klas = "table_cell2";
		}

		$que1 = "SELECT *,pdam_gart.getMerkMeter(wm_merk) AS merk_ket,TIMESTAMPDIFF(MINUTE,submit_tgl,'".$row0['remark_tgl']."') AS ref FROM caterpdam.v_ganti_meter WHERE client_id='".$row0['pel_no']."'";
	        try{
	                if(!$res1 = mysql_query($que1,$link)){
	                    throw new Exception($que1);
	                }
	                else{
	                    $row1 = mysql_fetch_array($res1);
	                    $mess = false;
	                }
	        }
	        catch (Exception $e){
	            $mess = $e->getMessage();
	            errorLog::errorDB(array($mess));
	            errorLog::logDB(array($que1));
	        }

?>
  <tr class="<?php echo $klas; ?>">
    <td class="right prn_cell"><?php echo number_format($nomor); 			?></td>
	<td class="right prn_cell"><?php echo $row0['pel_no']; 				?></td>
	<td class="left prn_cell prn_left"><?php echo $row0['pel_nama'];		?></td>
	<td class="left prn_cell"><?php echo $row0['pel_alamat'];			?></td>
	<td class="right prn_cell"><?php echo "-";	 				?></td>
	<td class="right prn_cell"><?php echo "-";				 	?></td>
	<td class="right prn_cell"><?php echo number_format($row0['rek_stanlalu']);	?></td>
	<td class="right prn_cell"><?php echo $row1['merk_ket'];	                ?></td>
	<td class="right prn_cell"><?php echo $row1['wm_no'];		                ?></td>
	<td class="right prn_cell"><?php echo $row1['stand_pasang'];    		?></td>
	<td class="right prn_cell"><?php echo substr($row0['remark_tgl'],0,10);		?></td>
	<td class="right prn_cell"><?php echo $row0['kps_ket']; 			?></td>
  </tr>

<?php
   		}
		if($i>0){
			for($j=0;$j<count($periode);$j++){
?>
	<tr class="table_cont_btm">
    	<td colspan="3" class="right prn_total"> Total Bulan :</td>
		<td class="right prn_total"><?php echo $periode[$j]; ?></td>
		<td class="right prn_total"><?php echo number_format(array_sum($lembar[$periode[$j]])); ?></td>
		<td class="right prn_total"><?php echo number_format(array_sum($uangair_awal[$periode[$j]])); ?></td>
		<td class="right prn_total"><?php echo number_format(array_sum($total_awal[$periode[$j]])); ?></td>
		<td class="right prn_total"></td>
	 	<td class="right prn_total"><?php echo number_format(array_sum($uangair_akhir[$periode[$j]])); ?></td>
   		<td class="right prn_total"><?php echo number_format(array_sum($total_akhir[$periode[$j]])); ?></td>
	 	<td class="right prn_total"><?php echo number_format(array_sum($uangair_selisih[$periode[$j]])); ?></td>
   		<td class="right prn_total"><?php echo number_format(array_sum($uangair_selisih[$periode[$j]])); ?></td>
	</tr>
<?php
			}
		}
?>
</table>
</div>
</div>

