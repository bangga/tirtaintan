<?php
	if($erno) die();
	$kar_id = _USER;
	if(strlen($dkd_kd) == 4){
		$dkd_kd = $_SESSION['Kota_c'].$dkd_kd;
	}
	
	/** koneksi ke database */
	$db		= false;
	try {
		$db 	= new PDO($PSPDO[0],$PSPDO[1],$PSPDO[2]);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch (PDOException $err){
		$mess = $err->getMessage();
		errorLog::errorDB(array($mess));
		$mess = "Mungkin telah terjadi kesalahan pada database server, sehingga koneksi tidak bisa dilakukan.";
		$klas = "error";
	}
	
	switch($proses){
		case 'updateNama':
			if($db and $pel_nama!=$remark_nama){
				try {
					$db->beginTransaction();
					$que	= "UPDATE tm_pelanggan SET pel_nama='".$remark_nama."',kar_id='"._USER."',remark_id='"._TOKN."',kps_kode=6 WHERE pel_no='".$pel_no."' AND kps_kode=0";
					$st 	= $db->exec($que);
					if($st>0){
						$db->commit();
						//$db->rollBack();
						errorLog::logDB(array($que));
						$mess = "Perubahan data pelanggan : ".$pel_no." telah di simpan.";
						$klas = "success";
					}
					else{
						$mess = "Tidak ada perubahan data pelanggan : ".$pel_no.".";
						$klas = "notice";
					}
				}
				catch (PDOException $err){
					$mess = $err->getMessage();
					errorLog::errorDB(array($mess));
					errorLog::logDB(array($que));
					$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses mutasi nama : ".$pel_no." tidak bisa dilakukan.";
					$klas = "error";
				}
			}
			else{
                                $mess = "Tidak ada perubahan data pelanggan : ".$pel_no.".";
                                $klas = "notice";
			}
			break;
		default:
			$mess = "Mungkin telah terjadi kesalahan pada prosedur manual, sehingga tidak ada proses yang bisa dijalankan.";
			$klas = "notice";
	}
	errorLog::logMess(array($mess));
	echo "<div class='".$klas."'>".$mess."</div>";
?>

