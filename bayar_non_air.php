<?php
        if($erno) die();
        $kar_id         = _USER;
        $byr_no         = _TOKN;
        $kopel          = $_SESSION['kp_ket'];
        $errorId        = false;

        /** koneksi ke database */
        $db             = false;
        try {
                $db     = new PDO($PSPDO[0],$PSPDO[1],$PSPDO[2]);
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                unset($mess);
        }
        catch (PDOException $err){
                $mess = $err->getTrace();
                errorLog::errorDB(array($mess[0]['args'][0]));
                $mess = "Mungkin telah terjadi kesalahan pada database server, sehingga koneksi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
                $klas = "error";
        }

        switch($proses){
                case "setResi":
                        $db->beginTransaction();
                        if($db){
                                try {
                                        $que    = "INSERT INTO system_parameter(sys_param,sys_value,sys_value1,sys_value2) VALUES('RESI','$kar_id','$noresi','-') ON DUPLICATE KEY UPDATE sys_value1='$noresi'";
                                        $st     = $db->exec($que);
                                        if($st>0){
                                                $db->commit();
                                                errorLog::logDB(array($que));
                                                $mess = "Set resi: $noresi telah dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
                                        }
                                        else{
                                                $db->rollBack();
                                                $mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses set resi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
                                        }
                                }
                                catch (PDOException $err){
                                        $db->rollBack();
                                        $mess = $err->getTrace();
                                        errorLog::errorDB(array($mess[0]['args'][0]));
                                        $mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses set resi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
                                }
                        }
                        break;
                case "bayar":
                        try{
                                $wsdl_url       = "http://"._PRIN."/printClient/printServer.wsdl";
                                $client         = new SoapClient($wsdl_url, array('cache_wsdl' => WSDL_CACHE_NONE) );
                                $cetak          = true;
                        }
                        catch (Exception $e){
                                $mess           = "Perangkat pencetak belum tersedia";
                                errorLog::errorDB(array($mess));
                                $mess           = $e->getMessage();
                                $klas           = "error";
                                $cetak          = false;
                        }

                        if($cetak){
                                $db->beginTransaction();
                                $erno   = false;
                                if($kembalian>=0){
                                        try {
                                                $que    = "INSERT INTO tm_pembayaran_non_air(byr_no,byr_tgl,byr_serial,pel_no,kar_id,lok_ip,na_kode,byr_total,byr_cetak,byr_upd_sts,byr_sts) VALUES($byr_no,NOW(),'$noresi','$pel_no','$kar_id','$ipClient','$na_kode',$bayar,0,NOW(),1)";
                                                $st     = $db->exec($que);
                                                if($st>0){
                                                        $noresi++;
                                                        errorLog::logDB(array($que));
                                                }
                                                else{
                                                        $erno   = true;
                                                        $mess   = "Gagal insert tabel pembayaran";
                                                        $i      = count($pilih) + 1;
                                                }

						// biaya adm PK
						if($kps_kode==5){
							$que    = "UPDATE tm_pelanggan SET kps_kode=0 WHERE pel_no='".$pel_no."' AND kps_kode=5";
						}
						// biaya adm balik nama
						else if($kps_kode==6){
							$que	= "UPDATE tm_pelanggan SET kps_kode=0 WHERE pel_no='".$pel_no."' AND kps_kode=6";
						}
						else{
                                                	$que    = "UPDATE tm_pelanggan SET kps_kode=9 WHERE pel_no='".$pel_no."' AND kps_kode=8";
						}

                                                $st     = $db->exec($que);
                                                if($st>0){
                                                        errorLog::logDB(array($que));
                                                }
                                                else{
                                                        $mess   = "Gagal update status pelanggan";
                                                        $i      = count($pilih) + 1;
                                                }

                                                $que    = "UPDATE system_parameter SET sys_value1='$noresi' WHERE sys_param='RESI' AND sys_value='$kar_id'";
                                                $st     = $db->exec($que);
                                                if($st>0){
                                                        errorLog::logDB(array($que));
                                                }
                                                else{
                                                        $erno   = true;
                                                        $mess   = "Gagal update nomer resi";
                                                }

						$que    = "SELECT @e_pel_no AS e_pel_no";

						foreach ($db->query($que) as $row) {
							$e_pel_no       = $row['e_pel_no'];
						}

						// line untuk ff continous paper
						$stringCetak  = chr(27).chr(67).chr(3);
						// enable paper out sensor
						$stringCetak .= chr(27).chr(57);
						// draft mode
						$stringCetak .= chr(27).chr(120).chr(48);
						// line spacing x/72
						$stringCetak .= chr(27).chr(65).chr(12);
						$stringCetak .= chr(10).chr(10).chr(10).chr(10);
						$stringCetak .= "TAGIHAN     : ".printRight("BP",16).  "  TAGIHAN     : ".$na_ket.chr(10);
						$stringCetak .= "BULAN       : ".printRight($bulan[date('n')]." ".date('Y'),16).                                           "  BULAN       : ".$bulan[date('n')]." ".date('Y').chr(10);
						$stringCetak .= "NO.REGISTER : ".printRight($pel_no,16).                                          "  NO.REGISTER : ".$pel_no.chr(10);
						$stringCetak .= "NO.SL       : ".printRight($e_pel_no,16).                            "  NO.SL       : ".$e_pel_no.chr(10);
						$stringCetak .= "NAMA        : ".printRight(substr($pel_nama,0,16),16).                   "  NAMA        : ".$pel_nama.chr(10);
						$stringCetak .= "ALAMAT      : ".printRight(substr($pel_alamat,0,16),16).               "  ALAMAT      : ".$pel_alamat.chr(10);
						$stringCetak .= "              ".printRight("",16).               "                ".chr(10);
						$stringCetak .= "              ".printRight("",16).                  "                ".chr(10);
						$stringCetak .= "              ".printRight("",16).                "  JML.TAGIHAN : ".printRight(number_format($bayar),10).chr(10);
						$stringCetak .= "              ".printRight("",16).                  "  TERBILANG   : ".strtoupper(substr((n2c($bayar,"Rupiah")),0,33)).chr(10);
						$stringCetak .= "              ".printRight("",16).               "                ".strtoupper(substr((n2c($bayar,"Rupiah")),33,33)).chr(10);
						$stringCetak .= "              ".printRight("",16).                  "                ".strtoupper(substr((n2c($bayar,"Rupiah")),66,33)).chr(10);
						$stringCetak .= "JML.TAGIHAN : ".printRight(number_format($bayar),16).                  "                ".strtoupper(substr((n2c($bayar,"Rupiah")),99,33)).chr(10);
						$stringCetak .= "REFERENSI   : ".$byr_no.                                                              "  REFERENSI   : ".$loket.$byr_no.chr(10);
						$stringCetak .= "KASIR       : ".printRight($noresi."-".$kar_id,16).                                   "  KASIR       : ".$noresi."-".$kar_id.chr(10);
						if($rek_materai[$i]>0){
							$stringCetak .= printRight("Mat. ".$rek_materai,77);
						}
						$stringCetak .= chr(12);
						// commit status proses transaksi
						$j = 1;
						if($erno){
							$db->rollBack();
							$j = 0;
						}
						else{
							$db->commit();
							//$db->rollBack();
						}
                                        }
                                        catch (PDOException $e){
                                                $erno   = true;
                                                $mess   = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses pembayaran tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
                                                $klas   = "error";
                                                errorLog::errorDB(array($e->getMessage()));
                                        }

                                        if(!$erno){
                                                try {
                                                        $stringFile       = $byr_no.".txt";
                                                        $openFile         = fopen("_data/".$stringFile, 'w');
                                                        fwrite($openFile, $stringCetak);
                                                        fclose($openFile);
                                                        $client->cetak(base64_encode($stringCetak),$stringFile);
                                                }
                                                catch (Exception $e) {
                                                        $mess   = "Proses cetak resi bayar gagal dilakukan";
                                                        errorLog::errorDB(array($mess));
                                                        $mess   = $e->getMessage();
                                                        $klas   = "error";
                                                }
                                        }

                                        if($j>0){
                                                $mess   = "$j transaksi telah dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
                                                $klas   = "success";
                                        }
                                        else{
                                                $mess   = "Transaksi tidak dapat dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
                                                $klas   = "error";
                                        }
                                }
                                else{
                                        $mess   = "Uang yang diterima tidak mencukupi. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
                                        $klas   = "error";
                                }
                        }
                        break;
                default:
                        $mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses pembayaran tidak bisa dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
                        $klas = "error";
        }
        errorLog::logMess(array($mess));
        echo "<input type=\"hidden\" id=\"$errorId\" value=\"$mess\"/>";
        unset($db);
?>

